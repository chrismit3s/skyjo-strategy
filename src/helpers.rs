use std::cmp::Ordering;


pub fn argmin(x: &[f64]) -> Vec<f64> {
    let min_index = x.iter().enumerate().min_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap_or(Ordering::Equal)).unwrap().0;
    (0..x.len()).into_iter().map(|i| if i == min_index { 1.0 } else { 0.0 }).collect()
}

pub fn softmin(x: &[f64]) -> Vec<f64> {
    let min = x.iter().min_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal)).unwrap();
    let y: Vec<f64> = x.iter().map(|a| (min - a).exp()).collect();
    let sum: f64 = y.iter().sum();
    y.iter().map(|a| a / sum).collect()
}

pub fn linearmin(x: &[f64]) -> Vec<f64> {
    let y: Vec<f64> = x.iter().map(|a| if *a <= 0.0 { 1.0 - a } else { 1.0 / (a + 1.0) }).collect();
    let sum: f64 = y.iter().sum();
    y.iter().map(|a| a / sum).collect()
}

pub fn elumin(x: &[f64]) -> Vec<f64> {
    let y: Vec<f64> = x.iter().map(|a| a.exp().ln_1p()).collect();
    let sum: f64 = y.iter().sum();
    y.iter().map(|a| a / sum).collect()
}

pub fn relumin(x: &[f64]) -> Vec<f64> {
    let y: Vec<f64> = x.iter().map(|a| 1.0 - a.min(0.0)).collect();
    let sum: f64 = y.iter().sum();
    y.iter().map(|a| a / sum).collect()
}

pub fn squaremin(x: &[f64]) -> Vec<f64> {
    let y: Vec<f64> = x.iter().map(|a| if *a <= 0.0 { (a - 1.0).powi(2) } else { 1.0 / (a + 1.0).powi(2) }).collect();
    let sum: f64 = y.iter().sum();
    y.iter().map(|a| a / sum).collect()
}

pub fn lnmin(x: &[f64]) -> Vec<f64> {
    let y: Vec<f64> = x.iter().map(|a| if *a <= 0.0 { (-a).ln_1p() + 1.0 } else { 1.0 / (1.0 + a) }).collect();
    let sum: f64 = y.iter().sum();
    y.iter().map(|a| a / sum).collect()
}

pub fn avgmin(x: &[f64]) -> Vec<f64> {
    let n = x.len() as f64;
    (0..x.len()).into_iter().map(|_| 1.0 / n).collect()
}
