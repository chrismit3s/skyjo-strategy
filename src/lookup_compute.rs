extern crate clap;
extern crate indicatif;
extern crate rayon;

use clap::{App, ArgMatches};
use crate::{Col, Hand, Lookup};
use indicatif::{ProgressBar, ProgressStyle};
use rayon::prelude::*;
use std::collections::HashMap;


pub trait LookupCompute {
    const DELTA: f64 = 0.01;
    const N: usize = 3000;

    // weird name, but this is the maximum number of rounds a game takes at least
    const MAX_MIN_ROUNDS: usize = Col::SIZE * Hand::SIZE;


    fn subcommand<'a, 'b>() -> App<'a, 'b>;

    fn from_matches(matches: &ArgMatches) -> Result<Self, String> where Self: Sized;

    fn call(&self, col: &Col) -> f64;

    fn update(&mut self, _lookup: &Lookup) {}

    fn compute_once(&mut self) -> Lookup where Self: Sync {
        let mut new_lookup = HashMap::with_capacity(Col::iter_len());
        new_lookup.par_extend(Col::iter().collect::<Vec<_>>()
                .par_iter()
                .map(|col| (col.clone(), self.call(col))));
        Lookup::new_simple(new_lookup)
    }

    fn compute_to_delta(&mut self, style: ProgressStyle) -> Lookup where Self: Sync {
        let progbar = ProgressBar::new(Self::N as u64).with_style(style);

        let mut old_lookup: Lookup;
        let mut new_lookup = self.compute_once();
        let mut delta = f64::INFINITY;
        while delta > Self::DELTA {
            old_lookup = new_lookup;
            new_lookup = self.compute_once();
            self.update(&new_lookup);

            delta = old_lookup.delta(&new_lookup).abs();

            progbar.set_message(&format!("current delta: {:.4}", delta));
            progbar.set_position((Self::N as f64 * Self::DELTA / delta) as u64);
        }

        progbar.finish();

        new_lookup
    }

    fn compute_n_times(&mut self, style: ProgressStyle) -> Lookup where Self: Sync {
        let progbar = ProgressBar::new(Self::N as u64).with_style(style);

        let mut old_lookup: Lookup;
        let mut new_lookup = self.compute_once();
        let mut delta: f64;
        for _ in 0..Self::N {
            old_lookup = new_lookup;
            new_lookup = self.compute_once();
            self.update(&new_lookup);

            delta = old_lookup.delta(&new_lookup).abs();

            progbar.set_message(&format!("current delta: {:.4}", delta));
            progbar.inc(1);
        }

        progbar.finish();

        new_lookup
    }

    fn compute(&mut self, _style: ProgressStyle) -> Lookup where Self: Sync {
        self.compute_once()
    }
}


mod best;
mod expected;
mod graphflow;
mod lastroundexp;
mod merged;
mod persistent;
mod reverseexpect;
mod regf;
mod simulated;

pub use best::Best;
pub use expected::Expected;
pub use graphflow::GraphFlow;
pub use lastroundexp::LastRoundExp;
pub use merged::Merged;
pub use persistent::Persistent;
pub use reverseexpect::ReverseExpect;
pub use regf::REGF;
pub use simulated::Simulated;
