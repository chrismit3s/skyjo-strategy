extern crate clap;

use clap::{App, ArgMatches, SubCommand};
use crate::{Col, Lookup, lookup_compute::LookupCompute};


pub struct Expected;

impl LookupCompute for Expected {
    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("expected")
    }

    fn from_matches(_matches: &ArgMatches) -> Result<Self, String> {
        Ok(Expected)
    }

    fn call(&self, _col: &Col) -> f64 {
        panic!("`call` called on `Expected`, use `compute_once` instead");
    }

    fn compute_once(&mut self) -> Lookup {
        Lookup::expected()
    }
}
