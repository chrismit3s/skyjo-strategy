extern crate clap;

use clap::{App, Arg, ArgMatches, SubCommand};
use crate::{Col, Lookup, lookup_compute::LookupCompute};


pub enum MergeType {
    Min,
    Max,
    Weighted(f64),
}


impl MergeType {
    fn merge(&self, a: f64, b: f64) -> f64 {
        match self {
            MergeType::Min => a.min(b),
            MergeType::Max => a.max(b),
            MergeType::Weighted(x) => a * x + b * (1.0 - x),
        }
    }
}


pub struct Merged {
    lookup1: Lookup,
    lookup2: Lookup,
    method: MergeType,
}


impl Merged {
    pub fn new(lookup1: Lookup, lookup2: Lookup, method: MergeType) -> Self {
        Merged { lookup1, lookup2, method }
    }
}


impl LookupCompute for Merged {
    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("merged")
                .arg(Arg::with_name("lookup1")
                    .short("1")
                    .long("lookup1")
                    .takes_value(true)
                    .required(true)
                    .help("the first lookup"))
                .arg(Arg::with_name("lookup2")
                    .short("2")
                    .long("lookup2")
                    .takes_value(true)
                    .help("the second lookup"))
                .arg(Arg::with_name("method")
                    .short("m")
                    .long("method")
                    .takes_value(true)
                    .required(true)
                    .help("the method used for merging"))
    }

    fn from_matches(matches: &ArgMatches) -> Result<Self, String> {
        let lookup1: Lookup = Lookup::from_name(matches.value_of("lookup1").unwrap())?;
        let lookup2: Lookup = if let Some(lookup_name) = matches.value_of("lookup2") {
            Lookup::from_name(lookup_name)?
        }
        else {
            Lookup::expected()
        };

        // parse func
        let method_arg = matches.value_of("method").unwrap();
        let method = if method_arg == "max" {
            MergeType::Max
        }
        else if method_arg == "min" {
            MergeType::Min
        }
        else if let Ok(weight) = method_arg.parse::<f64>() {
            MergeType::Weighted(weight)
        }
        else {
            return Err(format!("Unknown method argument {:?}", method_arg));
        };

        Ok(Self::new(lookup1, lookup2, method))
    }

    fn call(&self, col: &Col) -> f64 {
        self.method.merge(self.lookup1.col_score(col, 0), self.lookup2.col_score(col, 0))
    }
}
