extern crate clap;
extern crate indicatif;

use clap::{App, Arg, ArgMatches, SubCommand};
use crate::{Card, Col, Lookup, lookup_compute::LookupCompute};
use indicatif::ProgressStyle;


pub struct Persistent {
    lookup: Lookup,
    persistence: f64,
}


impl Persistent {
    pub fn new(lookup: Lookup, persistence: f64) -> Self {
        Persistent { lookup, persistence }
    }
}


impl LookupCompute for Persistent {
    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("persistence")
                .arg(Arg::with_name("lookup")
                    .short("l")
                    .long("lookup")
                    .takes_value(true)
                    .help("the initial lookup"))
                .arg(Arg::with_name("persistence")
                    .short("p")
                    .long("persistence")
                    .takes_value(true)
                    .required(true)
                    .help("the score persistence"))
    }

    fn from_matches(matches: &ArgMatches) -> Result<Self, String> {
        let lookup: Lookup = if let Some(lookup_name) = matches.value_of("lookup") {
            Lookup::from_name(lookup_name)?
        }
        else {
            Lookup::expected()
        };

        let persistence: f64 = matches.value_of("persistence").unwrap().parse().map_err(|_| "Invalid persistence argument".to_string())?;

        Ok(Self::new(lookup, persistence))
    }

    fn call(&self, col: &Col) -> f64 {
        // col of cards of the same value cancels
        if col.is_triple() {
            return 0.0;
        }

        let mut col = col.clone();

        // this is a rough approximation for how many unknowns would be in the hand of the
        // player with the least unknowns, so also a rough approx for how many rounds it
        // will take for the game to end
        let min_rounds = 3 * col.count(Card::Unknown);

        // compute score after flipping a card (if there are any cards to flip)
        // lower score is better, so this is the worst score, will definitely be overwritten
        let flip_score = col.score_after_flip(&self.lookup, min_rounds).unwrap_or(f64::INFINITY);

        // compute score after drawing a card
        let draw_score = Card::weighted_average(|card| flip_score.min(col.best_score_after_replace(card, &self.lookup, min_rounds).1));

        // compute score after swapping a card
        let swap_score = Card::weighted_average(|card| draw_score.min(col.best_score_after_replace(card, &self.lookup, min_rounds).1));

        self.persistence * self.lookup.col_score(&col, min_rounds) + (1.0 - self.persistence) * swap_score
    }

    fn update(&mut self, lookup: &Lookup) {
        self.lookup = lookup.clone();
    }

    fn compute(&mut self, style: ProgressStyle) -> Lookup {
        self.compute_to_delta(style)
    }
}
