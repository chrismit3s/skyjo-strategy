extern crate clap;
extern crate indicatif;

use clap::{App, Arg, ArgMatches, SubCommand};
use crate::{Card, Col, Lookup, lookup_compute::LookupCompute};
use indicatif::ProgressStyle;


pub struct Best {
    lookup: Lookup,
    depth: usize,
}


impl Best {
    pub fn new(lookup: Lookup, depth: usize) -> Self {
        Best { lookup, depth }
    }

    fn call_rec(&self, col: &mut Col, depth: usize) -> f64 {
        // col of cards of the same value cancels
        if col.is_triple() {
            return 0.0;
        }

        // this is a rough approximation for how many unknowns would be in the hand of the
        // player with the least unknowns, so also a rough approx for how many rounds it
        // will take for the game to end
        let min_rounds = 3 * col.count(Card::Unknown);

        if depth == 0 {
            return self.lookup.col_score(col, min_rounds);
        }

        // compute score after flipping a card (if there are any cards to flip)
        // lower score is better, so this is the worst score, will definitely be overwritten
        let flip_score = col.score_after_flip(&self.lookup, min_rounds).unwrap_or(f64::INFINITY);

        // compute score after drawing a card, if its worse than the flip score we use it instead
        let draw_score = Card::weighted_average(|new_card| {
            // swapping a card has to be better than this score, else we flip not swap
            let mut curr_best_score = flip_score;
            for old_card in col.iter_all_different().collect::<Vec<_>>() {
                // do
                col.replace_with(old_card, new_card).unwrap();

                // use best move for the current card
                curr_best_score = curr_best_score.min(self.call_rec(col, depth - 1));

                // undo
                col.replace_with(new_card, old_card).unwrap();
            }

            curr_best_score
        });

        // compute score after drawing a card, if its worse than the flip score we use it instead
        Card::weighted_average(|new_card| {
            // swapping a card has to be better than this score, else we flip not swap
            let mut curr_best_score = draw_score;
            for old_card in col.iter_all_different().collect::<Vec<_>>() {
                // do
                col.replace_with(old_card, new_card).unwrap();

                // use best move for the current card
                curr_best_score = curr_best_score.min(self.call_rec(col, depth - 1));

                // undo
                col.replace_with(new_card, old_card).unwrap();
            }

            curr_best_score
        })
    }
}

impl LookupCompute for Best {
    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("best")
                .arg(Arg::with_name("lookup")
                    .short("l")
                    .long("lookup")
                    .takes_value(true)
                    .help("the initial lookup"))
                .arg(Arg::with_name("depth")
                    .short("d")
                    .long("depth")
                    .takes_value(true)
                    .required(true)
                    .help("the recursion depth"))
    }

    fn from_matches(matches: &ArgMatches) -> Result<Self, String> {
        let lookup: Lookup = if let Some(lookup_name) = matches.value_of("lookup") {
            Lookup::from_name(lookup_name)?
        }
        else {
            Lookup::expected()
        };

        let depth: usize = matches.value_of("depth").unwrap().parse().map_err(|_| "Invalid depth argument".to_string())?;

        Ok(Self::new(lookup, depth))
    }

    fn call(&self, col: &Col) -> f64 {
        self.call_rec(&mut col.clone(), self.depth)
    }

    fn update(&mut self, lookup: &Lookup) {
        self.lookup = lookup.clone();
    }

    fn compute(&mut self, style: ProgressStyle) -> Lookup {
        self.compute_to_delta(style)
    }
}
