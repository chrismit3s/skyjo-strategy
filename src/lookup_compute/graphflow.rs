extern crate arrayvec;
extern crate clap;
extern crate indicatif;

use arrayvec::ArrayVec;
use clap::{Arg, App, ArgMatches, SubCommand};
use crate::{Card, Col, Lookup, helpers, lookup_compute::LookupCompute};
use indicatif::ProgressStyle;
use std::collections::HashMap;


struct Node {
    score: f64,

    // the ith entry represents the col after flipping to the card with index i,
    // None if the col has no unknown to flip
    flip_neighbors: Option<[Col; Card::NUM_CARDS]>,

    // the ith entry represents all possible reachable cols after replacing
    // any card with the card with index i
    replace_neighbors: [Vec<Col>; Card::NUM_CARDS],
}


pub struct GraphFlow {
    weight_func: &'static (dyn Fn(&[f64]) -> Vec<f64> + Sync),
    score_persist: f64,
    include_noop: bool,
    nodes: HashMap<Col, Node>,
}


impl GraphFlow {
    const GAMMA: f64 = 0.01;

    pub fn new(weight_func: String, score_persist: f64, include_noop: bool) -> Self {
        let weight_func: &'static fn(&[f64]) -> Vec<f64> = match weight_func.as_str() {
            "argmin"    => &(helpers::argmin as fn(&[f64]) -> Vec<f64>),
            "avgmin"    => &(helpers::avgmin as fn(&[f64]) -> Vec<f64>),
            "elumin"    => &(helpers::elumin as fn(&[f64]) -> Vec<f64>),
            "linearmin" => &(helpers::linearmin as fn(&[f64]) -> Vec<f64>),
            "lnmin"     => &(helpers::lnmin as fn(&[f64]) -> Vec<f64>),
            "relumin"   => &(helpers::relumin as fn(&[f64]) -> Vec<f64>),
            "softmin"   => &(helpers::softmin as fn(&[f64]) -> Vec<f64>),
            "squaremin" => &(helpers::squaremin as fn(&[f64]) -> Vec<f64>),
            _ => panic!("Unknown weightfunc {}", weight_func),
        };

        let mut graph = GraphFlow { weight_func, score_persist, include_noop, nodes: HashMap::with_capacity(Col::iter_len()) };

        for col in Col::iter() {
            // compute the flip neighbors
            let flip_neighbors = if col.has_unknowns() {
                let arr: ArrayVec<[Col; Card::NUM_CARDS]> = Card::iter_values()
                        .map(|card| col.after_flip_to(card))
                        .collect::<Result<_, _>>()
                        .unwrap();
                Some(arr.into_inner().unwrap())
            }
            else {
                None
            };

            // compute the draw neighbors
            let arr: ArrayVec<[Vec<Col>; Card::NUM_CARDS]> = Card::iter_values()
                    .map(|card| col.all_after_replace(card))
                    .collect::<Result<_, _>>()
                    .unwrap();
            let replace_neighbors = arr.into_inner().unwrap();

            let node = Node { score: col.expected_score(), flip_neighbors, replace_neighbors };
            graph.nodes.insert(col.clone(), node);
        }

        graph
    }
}

impl LookupCompute for GraphFlow {
    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("graphflow")
                .arg(Arg::with_name("weightfunc")
                    .short("w")
                    .long("weight")
                    .takes_value(true)
                    .possible_values(&["argmin",  "softmin", "linearmin", "elumin", "relumin", "squaremin", "avgmin", "lnmin"])
                    .required(true)
                    .help("the used weighting function for all reachable scores for a node"))
                .arg(Arg::with_name("persistence")
                    .short("p")
                    .long("persist")
                    .takes_value(true)
                    .default_value("0.0")
                    .help("how much of the score of known columns should be kept"))
                .arg(Arg::with_name("include-noop")
                    .short("n")
                    .long("include-noop")
                    .takes_value(false)
                    .help("whether or not the current score of a node should be included in the possible scores"))

    }

    fn from_matches(matches: &ArgMatches) -> Result<Self, String> {
        let weight_func: String = matches.value_of("weightfunc").unwrap().to_string();
        let score_persist: f64 = matches.value_of("persistence").unwrap().parse().map_err(|_| "Invalid persistence argument".to_string())?;
        let include_noop: bool = matches.is_present("include-noop");

        Ok(Self::new(weight_func, score_persist, include_noop))
    }

    fn call(&self, _col: &Col) -> f64 {
        panic!("`call` called on `GraphFlow`, use `compute_once` instead");
    }

    fn update(&mut self, lookup: &Lookup) {
        for (col, node) in self.nodes.iter_mut() {
            node.score = lookup.col_score(col, 0);
        }
    }

    fn compute_once(&mut self) -> Lookup {
        let mut lookup: HashMap<Col, f64> = HashMap::with_capacity(Col::iter_len());

        // create new lookup without changing graph values
        for (col, node) in self.nodes.iter() {
            // skip triples
            if col.is_triple() {
                lookup.insert(*col, 0.0);
                continue;
            }

            // average score after flipping a card if possible
            let flip_score = node.flip_neighbors.map(
                |flip_neighbors| Card::weighted_average(
                    |card| self.nodes[&flip_neighbors[card.index().unwrap()]].score));

            // average score after making the best move after drawing a card
            let draw_score = Card::weighted_average(|card| {
                let neighbors = &node.replace_neighbors[card.index().unwrap()];
                let mut scores: Vec<_> = neighbors.iter().map(|col| self.nodes[col].score).collect();
                if let Some(flip_score) = flip_score {
                    scores.push(flip_score);
                }

                let weights = (self.weight_func)(&scores.iter().map(|score| score - node.score).collect::<Vec<_>>());

                scores.iter().zip(weights)
                    .map(|(score, weight)| score * weight)
                    .sum()
            });

            // average score after making the best move for any center card
            let mut score = Card::weighted_average(|card| {
                let neighbors = &node.replace_neighbors[card.index().unwrap()];
                let mut scores: Vec<_> = neighbors.iter().map(|col| self.nodes[col].score).collect();
                scores.push(draw_score);
                if self.include_noop {
                    scores.push(node.score);  // noop, aka dont change anything
                }

                let weights = (self.weight_func)(&scores.iter().map(|score| score - node.score).collect::<Vec<_>>());

                scores.iter().zip(weights)
                    .map(|(score, weight)| score * weight)
                    .sum()
            });

            score = (1.0 - Self::GAMMA) * node.score + Self::GAMMA * score;
            if !col.has_unknowns() {
                score = (1.0 - self.score_persist) * score + self.score_persist * col.score().unwrap();
            }
            lookup.insert(*col, score);
        }

        Lookup::new_simple(lookup)
    }

    fn compute(&mut self, style: ProgressStyle) -> Lookup {
        self.compute_n_times(style)
    }
}
