extern crate arrayvec;
extern crate clap;
extern crate indicatif;
extern crate rayon;

use arrayvec::ArrayVec;
use clap::{App, Arg, ArgMatches, SubCommand};
use crate::{Card, Col, Hand, Lookup, lookup_compute::LookupCompute};
use rayon::prelude::*;
use std::{iter, collections::{HashMap, HashSet}};
use indicatif::ProgressStyle;


struct Avg {
    value_sum: f64,
    weight_sum: f64,
}

impl Avg {
    pub fn new() -> Avg {
        Avg { value_sum: 0.0, weight_sum: 0.0 }
    }

    pub fn value(&self) -> f64 {
        if self.weight_sum == 0.0 {
            0.0
        }
        else {
            self.value_sum / self.weight_sum
        }
    }

    pub fn add(&mut self, value: f64, weight: f64) {
        self.value_sum += value * weight;
        self.weight_sum += weight;
    }

    pub fn combine(&mut self, other: &Self) {
        self.value_sum += other.value_sum;
        self.weight_sum += other.weight_sum;
    }
}


pub struct Simulated {
    lookup: Lookup,
    batch_size: usize,
    exponent: f64,
    max_rounds: Option<usize>,
}


impl Simulated {
    const GAMMA: f64 = 0.01;

    pub fn new(lookup: Lookup, batch_size: usize, exponent: f64, max_rounds: Option<usize>) -> Self {
        Simulated { lookup, batch_size, exponent, max_rounds }
    }

    fn play_once(&self) -> [(HashSet<Col>, f64, usize); Hand::SIZE] {
        let cols_hist: ArrayVec<[(HashSet<Col>, f64, usize); Hand::SIZE]> =
            iter::repeat_with(|| (HashSet::<Col>::new(), 0.0, 0)).take(Hand::SIZE).collect();

        // array that contains a hashset of all columns that were once in that column in that hand
        // and the final score of that column
        let mut cols_hist = cols_hist.into_inner().unwrap();

        // a vector that maps the index of the current cols in the hand to the index in the array
        // above
        let mut index_map: Vec<usize> = (0..Hand::SIZE).collect();

        let mut hand = Hand::unknown(&self.lookup);
        let mut round = 0;
        while !hand.finished() && self.max_rounds.map_or(true, |val| val < round) {
            round += 1;

            // this is a rough approximation for how many unknowns would be in the hand of the
            // player with the least unknowns, so also a rough approx for how many rounds it
            // will take for the game to end
            let mut min_rounds = hand.count_unknowns() - 2;
            min_rounds = self.max_rounds.map_or(min_rounds, |val| min_rounds.min(val - round));

            let ac = hand.apply_best_action(Card::random(), f64::INFINITY, min_rounds);
            let ac_index = ac.col_index();
            let mapped_index = index_map[ac_index];
            let tuple = &mut cols_hist[mapped_index];

            // a column got removed
            if index_map.len() != hand.cols.len() {
                // must be the one that changed that round
                index_map.remove(ac_index);
                tuple.1 = 0.0;
                tuple.2 = round;

                // at this point we would normally insert the removed column, which would be a
                // triple of some card, but its actually quite the hassle to get that column, and
                // since we actually know the value of a column like that to be zero we skip that
                // and just fix the values of those columns at zero (as they dont appear in any
                // HashSet, we wont update its value in the lookup, and that value starts at zero,
                // were it will stay the entire time)
            }
            else {
                tuple.0.insert(hand.cols[ac_index].clone());
            }
        }

        for (col, index) in hand.cols.iter().zip(index_map.iter()) {
            let tuple = &mut cols_hist[*index];
            tuple.1 = col.score().unwrap();
            tuple.2 = round;
        }

        cols_hist
    }
}

impl LookupCompute for Simulated {
    const N: usize = 500;

    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("simulated")
                .arg(Arg::with_name("lookup")
                    .short("l")
                    .long("lookup")
                    .takes_value(true)
                    .help("the initial lookup"))
                .arg(Arg::with_name("batch-size")
                    .short("b")
                    .long("batch-size")
                    .takes_value(true)
                    .required(true)
                    .help("the batch size"))
                .arg(Arg::with_name("max-rounds")
                    .short("r")
                    .long("rounds")
                    .takes_value(true)
                    .required(false)
                    .help("the maximum number of rounds a simulated game should take"))
                .arg(Arg::with_name("exponent")
                    .short("e")
                    .long("exp")
                    .takes_value(true)
                    .default_value("1.0")
                    .help("the exponent of the weighting function (the bigger, the shorter the rounds will be)"))
    }

    fn from_matches(matches: &ArgMatches) -> Result<Self, String> {
        let lookup: Lookup = if let Some(lookup_name) = matches.value_of("lookup") {
            Lookup::from_name(lookup_name)?
        }
        else {
            Lookup::expected()
        };

        let batch_size: usize = matches.value_of("batch-size").unwrap().parse().map_err(|_| "Invalid batch-size argument".to_string())?;
        let exponent: f64 = matches.value_of("exponent").unwrap().parse().map_err(|_| "Invalid exponent argument".to_string())?;
        let max_rounds: Option<usize> = matches.value_of("max-rounds").map(|val| val.parse().map_err(|_| "Invalid exponent argument".to_string())).transpose()?;

        Ok(Self::new(lookup, batch_size, exponent, max_rounds))
    }

    fn call(&self, _col: &Col) -> f64 {
        panic!("`call` called on `Simulated`, use `compute_once` instead");
    }

    fn compute_once(&mut self) -> Lookup {
        let mut lookup = self.lookup.get_simple_map(0);
        let avgs = (0..self.batch_size).into_par_iter()
            .flat_map_iter(|_| self.play_once().to_vec())
            .fold(|| HashMap::<Col, Avg>::new(), |mut m, (cols, score, round)| {
                for col in cols.iter() {
                    m.entry(*col).or_insert_with(|| Avg::new()).add(score, (round as f64).powf(-self.exponent));
                }
                m
            })
            .reduce(|| HashMap::<Col, Avg>::new(), |mut m, n| {
                for (col, avg) in n.iter() {
                    m.entry(*col).or_insert_with(|| Avg::new()).combine(avg);
                }
                m
            });

        for (col, avg) in avgs.into_iter() {
            lookup.insert(col, (1.0 - Self::GAMMA) * lookup[&col] + Self::GAMMA * avg.value());
        }

        Lookup::new_simple(lookup)
    }

    fn update(&mut self, lookup: &Lookup) {
        self.lookup = lookup.clone();
    }

    fn compute(&mut self, style: ProgressStyle) -> Lookup {
        self.compute_n_times(style)
    }
}
