extern crate clap;

use clap::{App, ArgMatches, SubCommand};
use crate::{Card, Col, Hand, Lookup, lookup_compute::LookupCompute};
use indicatif::ProgressStyle;


pub struct ReverseExpect {
    lookup: Lookup,
}


impl ReverseExpect {
    pub fn new() -> Self {
        let lookup = Lookup::expected();
        ReverseExpect { lookup }
    }
}


impl LookupCompute for ReverseExpect {
    // with every run we go back one round, so we start with expected
    // and step back 11 times (assuming a 3x4 hand)
    const N: usize = Col::SIZE * Hand::SIZE - 1;

    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("reverseexpect")
    }

    fn from_matches(_matches: &ArgMatches) -> Result<Self, String> {
        Ok(Self::new())
    }

    fn call(&self, _col: &Col) -> f64 {
        panic!("`call` called on `ReverseExpect`, use `compute_once` instead");
    }

    fn compute_once(&mut self) -> Lookup {
        // the biggest value of min_rounds that is known in this lookup;
        // as the Vecs for each col all have the same length, we just use any one
        let known_min_rounds = self.lookup.get_scores(&Col::unknown()).len() - 1;

        // create new lookup without changing graph values
        for mut col in Col::iter() {
            // skip triples
            if col.is_triple() {
                self.lookup.get_scores_mut(&col).push(0.0);
                continue;
            }

            // average score after flipping a card
            let flip_score = col.score_after_flip(&self.lookup, known_min_rounds).unwrap_or(f64::INFINITY);

            // average score after making the best move after drawing a card
            let draw_score = Card::weighted_average(|card|
                flip_score.min(col.best_score_after_replace(card, &self.lookup, known_min_rounds).1));

            // average score after making the best move for any center card
            let score = Card::weighted_average(|card|
                draw_score.min(col.best_score_after_replace(card, &self.lookup, known_min_rounds).1));

            self.lookup.get_scores_mut(&col).push(score);
        }

        self.lookup.clone()
    }

    fn compute(&mut self, style: ProgressStyle) -> Lookup {
        self.compute_n_times(style)
    }
}
