extern crate arrayvec;
extern crate clap;
extern crate indicatif;

use arrayvec::ArrayVec;
use clap::{Arg, App, ArgMatches, SubCommand};
use crate::{Card, Col, Hand, Lookup, helpers, lookup_compute::LookupCompute};
use indicatif::ProgressStyle;
use std::collections::HashMap;


const EXP_RAMP_PARAMS: &'static [f64] = &[1.23, 2.67, 4.801, 9.995];  // TODO more sigfigs


fn expramp(x: f64, a: f64) -> f64 {
    ((a * x).exp() - 1.0) / (a.exp() - 1.0)
}

fn linearramp(x: f64) -> f64 {
    x
}

fn expramp_n4(x: f64) -> f64 { expramp(x, -EXP_RAMP_PARAMS[3]) }
fn expramp_n3(x: f64) -> f64 { expramp(x, -EXP_RAMP_PARAMS[2]) }
fn expramp_n2(x: f64) -> f64 { expramp(x, -EXP_RAMP_PARAMS[1]) }
fn expramp_n1(x: f64) -> f64 { expramp(x, -EXP_RAMP_PARAMS[0]) }
fn expramp_p1(x: f64) -> f64 { expramp(x, EXP_RAMP_PARAMS[0]) }
fn expramp_p2(x: f64) -> f64 { expramp(x, EXP_RAMP_PARAMS[1]) }
fn expramp_p3(x: f64) -> f64 { expramp(x, EXP_RAMP_PARAMS[2]) }
fn expramp_p4(x: f64) -> f64 { expramp(x, EXP_RAMP_PARAMS[3]) }


struct Node {
    scores: Vec<f64>,

    // the ith entry represents the col after flipping to the card with index i,
    // None if the col has no unknown to flip
    flip_neighbors: Option<[Col; Card::NUM_CARDS]>,

    // the ith entry represents all possible reachable cols after replacing
    // any card with the card with index i
    replace_neighbors: [Vec<Col>; Card::NUM_CARDS],
}


/// REGF = ReverseExpect Graphflow, a combination of both algorithms
pub struct REGF {
    weight_func: &'static (dyn Fn(&[f64]) -> Vec<f64> + Sync),

    // computes the probablity that the min_rounds argument is going to
    // decrease next round
    prob_func: &'static (dyn Fn(f64) -> f64 + Sync),

    // step size
    gamma: f64,
    nodes: HashMap<Col, Node>,
}


impl REGF {
    const GAMMA_INIT: f64 = 0.05;
    const GAMMA_DECAY: f64 = 0.999;

    pub fn new(weight_func: String, prob_func: String) -> Self {
        let weight_func: &'static fn(&[f64]) -> Vec<f64> = match weight_func.as_str() {
            "argmin"    => &(helpers::argmin as fn(&[f64]) -> Vec<f64>),
            "avgmin"    => &(helpers::avgmin as fn(&[f64]) -> Vec<f64>),
            "elumin"    => &(helpers::elumin as fn(&[f64]) -> Vec<f64>),
            "linearmin" => &(helpers::linearmin as fn(&[f64]) -> Vec<f64>),
            "lnmin"     => &(helpers::lnmin as fn(&[f64]) -> Vec<f64>),
            "relumin"   => &(helpers::relumin as fn(&[f64]) -> Vec<f64>),
            "softmin"   => &(helpers::softmin as fn(&[f64]) -> Vec<f64>),
            "squaremin" => &(helpers::squaremin as fn(&[f64]) -> Vec<f64>),
            _ => panic!("Unknown weightfunc {}", weight_func),
        };

        let prob_func: &'static fn(f64) -> f64 = match prob_func.as_str() {
            "expramp-n4"    => &(expramp_n4 as fn(f64) -> f64),
            "expramp-n3"    => &(expramp_n3 as fn(f64) -> f64),
            "expramp-n2"    => &(expramp_n2 as fn(f64) -> f64),
            "expramp-n1"    => &(expramp_n1 as fn(f64) -> f64),
            "linearramp"    => &(linearramp as fn(f64) -> f64),
            "expramp-p1"    => &(expramp_p1 as fn(f64) -> f64),
            "expramp-p2"    => &(expramp_p2 as fn(f64) -> f64),
            "expramp-p3"    => &(expramp_p3 as fn(f64) -> f64),
            "expramp-p4"    => &(expramp_p4 as fn(f64) -> f64),
            _ => panic!("Unknown probfunc {}", prob_func),

        };

        let mut graph = REGF { weight_func, prob_func, nodes: HashMap::with_capacity(Col::iter_len()), gamma: Self::GAMMA_INIT };

        // create graph structure and set the score at min_rounds=0 to the expected score
        for col in Col::iter() {
            // compute the flip neighbors
            let flip_neighbors = if col.has_unknowns() {
                let arr: ArrayVec<[Col; Card::NUM_CARDS]> = Card::iter_values()
                        .map(|card| col.after_flip_to(card))
                        .collect::<Result<_, _>>()
                        .unwrap();
                Some(arr.into_inner().unwrap())
            }
            else {
                None
            };

            // compute the draw neighbors
            let arr: ArrayVec<[Vec<Col>; Card::NUM_CARDS]> = Card::iter_values()
                    .map(|card| col.all_after_replace(card))
                    .collect::<Result<_, _>>()
                    .unwrap();
            let replace_neighbors = arr.into_inner().unwrap();

            let mut scores = Vec::with_capacity(Col::SIZE * Hand::SIZE + 1);
            scores.push(col.expected_score());

            let node = Node { scores, flip_neighbors, replace_neighbors };
            graph.nodes.insert(col.clone(), node);
        }

        // set the scores at min_rounds>0
        for min_rounds in 1..(Col::SIZE * Hand::SIZE + 1) {
            for col in Col::iter() {
                let score = graph.new_score_for(col, min_rounds - 1);
                let node = graph.nodes.get_mut(&col).unwrap();
                node.scores.push(score);
            }
        }

        graph
    }

    fn new_score_for(&self, col: Col, min_rounds: usize) -> f64 {
        let node = &self.nodes[&col];

        // skip triples
        if col.is_triple() {
            return 0.0;
        }

        // average score after flipping a card if possible
        let flip_score = node.flip_neighbors.map(
            |flip_neighbors| Card::weighted_average(
                |card| self.nodes[&flip_neighbors[card.index().unwrap()]].scores[min_rounds]));

        // average score after making the best move after drawing a card
        let draw_score = Card::weighted_average(|card| {
            let neighbors = &node.replace_neighbors[card.index().unwrap()];
            let mut scores: Vec<_> = neighbors.iter().map(|col| self.nodes[col].scores[min_rounds]).collect();
            if let Some(flip_score) = flip_score {
                scores.push(flip_score);
            }

            let weights = (self.weight_func)(&scores.iter().map(|score| score - node.scores[min_rounds]).collect::<Vec<_>>());

            scores.iter().zip(weights)
                .map(|(score, weight)| score * weight)
                .sum()
        });

        // return average score after making the best move for any center card
        Card::weighted_average(|card| {
            let neighbors = &node.replace_neighbors[card.index().unwrap()];
            let mut scores: Vec<_> = neighbors.iter().map(|col| self.nodes[col].scores[min_rounds]).collect();
            scores.push(draw_score);
            scores.push(node.scores[min_rounds]);  // noop, aka dont change anything

            let weights = (self.weight_func)(&scores.iter().map(|score| score - node.scores[min_rounds]).collect::<Vec<_>>());

            scores.iter().zip(weights)
                .map(|(score, weight)| score * weight)
                .sum()
        })
    }
}

impl LookupCompute for REGF {
    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("regf")
                .arg(Arg::with_name("weightfunc")
                    .short("w")
                    .long("weight")
                    .takes_value(true)
                    .possible_values(&["argmin",  "softmin", "linearmin", "elumin", "relumin", "squaremin", "avgmin", "lnmin"])
                    .required(true)
                    .help("the used weighting function for all reachable scores for a node"))
                .arg(Arg::with_name("probfunc")
                    .short("p")
                    .long("prob")
                    .takes_value(true)
                    .possible_values(&["expramp-n4", "expramp-n3", "expramp-n2", "expramp-n1", "linearramp", "expramp-p1", "expramp-p2", "expramp-p3", "expramp-p4"])
                    .required(true)
                    .help("the probablity for how likely it is for min_rounds to decrease next round"))

    }

    fn from_matches(matches: &ArgMatches) -> Result<Self, String> {
        let weight_func: String = matches.value_of("weightfunc").unwrap().to_string();
        let prob_func: String = matches.value_of("probfunc").unwrap().to_string();

        Ok(Self::new(weight_func, prob_func))
    }

    fn call(&self, _col: &Col) -> f64 {
        panic!("`call` called on `REGF`, use `compute_once` instead");
    }

    fn update(&mut self, lookup: &Lookup) {
        for (col, node) in self.nodes.iter_mut() {
            node.scores = lookup.get_scores(col).iter().map(|score| *score).collect();
        }
    }

    fn compute_once(&mut self) -> Lookup {
        let mut lookup: HashMap<Col, Vec<f64>> = HashMap::with_capacity(Col::iter_len());

        // set the score at min_rounds=0 to the expected score
        for col in Col::iter() {
            let mut scores: Vec<f64> = Vec::with_capacity(Col::SIZE * Hand::SIZE + 1);
            scores.push(col.expected_score());
            lookup.insert(col, scores);
        }

        // set the scores at min_rounds>0
        for min_rounds in 1..(Col::SIZE * Hand::SIZE + 1) {
            for col in Col::iter() {
                // the score if min_rounds decreases this round
                let score_after_decrease = self.new_score_for(col, min_rounds - 1);

                // the score if min_rounds doesnt decrease this round
                let score_no_decrease = self.new_score_for(col, min_rounds);

                // the probability min_rounds decreases this round
                let decrease_prob = (self.prob_func)(min_rounds as f64 / (Col::SIZE * Hand::SIZE) as f64);

                let new_score = decrease_prob * score_after_decrease + (1.0 - decrease_prob) * score_no_decrease;
                let old_score = self.nodes[&col].scores[min_rounds];
                let score = self.gamma * new_score + (1.0 - self.gamma) * old_score;

                lookup.get_mut(&col).unwrap().push(score);
            }
        }
        self.gamma *= Self::GAMMA_DECAY;

        Lookup::new_complex(lookup)
    }

    fn compute(&mut self, style: ProgressStyle) -> Lookup {
        self.compute_n_times(style)
    }
}
