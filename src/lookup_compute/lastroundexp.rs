extern crate clap;

use clap::{App, Arg, ArgMatches, SubCommand};
use crate::{Col, Lookup, lookup_compute::LookupCompute};


pub struct LastRoundExp {
    lookup: Lookup,
}


impl LastRoundExp {
    pub fn new(lookup: Lookup) -> Self {
        LastRoundExp { lookup }
    }
}


impl LookupCompute for LastRoundExp {
    fn subcommand<'a, 'b>() -> App<'a, 'b> {
        SubCommand::with_name("lastroundexp")
                .arg(Arg::with_name("lookup")
                    .short("l")
                    .long("lookup")
                    .takes_value(true)
                    .required(true)
                    .help("the lookup"))
    }

    fn from_matches(matches: &ArgMatches) -> Result<Self, String> {
        let lookup: Lookup = Lookup::from_name(matches.value_of("lookup").unwrap())?;
        Ok(Self::new(lookup))
    }

    fn call(&self, _col: &Col) -> f64 {
        panic!("`call` called on `LastRoundExp`, use `compute_once` instead");
    }

    fn compute_once(&mut self) -> Lookup {
        Col::iter().for_each(|col| self.lookup.get_scores_mut(&col).insert(0, col.expected_score()));
        self.lookup.clone()
    }
}
