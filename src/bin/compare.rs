#[macro_use]
extern crate clap;
extern crate indicatif;
extern crate rand;
extern crate rayon;

use clap::{App, Arg};
use indicatif::{ProgressBar, ProgressStyle, ParallelProgressIterator};
use rayon::prelude::*;
use skyjo::{Card, Hand, Lookup};
use std::{env, iter, cmp::Ordering, error::Error};


const N: usize = 20_000;


fn main() -> Result<(), Box<dyn Error>> {
    let progbar_style = ProgressStyle::default_bar()
            .progress_chars("=>.")
            .template("{percent:>3.0}% [{bar:60}] eta: {eta_precise} - {msg}");

    let n_string = format!("{}", N);
    let matches = App::new("skyjo-strat-perf")
            .version(crate_version!())
            .author("Christoph S <chrissidtmeier@gmail.com>")
            .about("Helps you win at Skyjo")
            .arg(Arg::with_name("lookups")
                .takes_value(true)
                .required(true)
                .help("the lookup to test"))
            .arg(Arg::with_name("n")
                .short("n")
                .takes_value(true)
                .default_value(&n_string)
                .help("the number of games to simulate"))
            .get_matches();

    // load lookups
    let names: Vec<_> = matches.values_of("lookups").unwrap().map(str::to_string).collect();
    let lookups: Vec<_> = names.iter().map(|s| Lookup::from_name(&s)).collect::<Result<_, _>>()?;
    println!("Read lookups");

    // get n
    let n: usize = matches.value_of("n").unwrap().parse()?;

    // compute
    let res = compare_n(lookups, n, progbar_style);

    let max_name_len = names.iter().map(String::len).max().unwrap();
    for (name, wins) in names.iter().zip(res.iter()) {
        println!("{:width$} {:2.4}%", name, 100.0 * wins.len() as f64 / n as f64, width = max_name_len);
    }

    Ok(())
}


fn compare(lookups: &[Lookup]) -> (usize, usize, f64) {  // (winner, rounds, score)
    let mut hands: Vec<Hand> = lookups.iter().map(|lookup| Hand::unknown(lookup)).collect();
    let mut unknown_counts: Vec<usize> = hands.iter().map(|hand| hand.count_unknowns()).collect();
    let mut min_unknown_count = *unknown_counts.iter().min().unwrap();

    let mut round = 0;
    let mut finishing_score: Option<f64> = None;
    let mut finishing_index: Option<usize> = None;
    'outer: loop {
        round += 1;
        //for (i, hand) in hands.iter_mut().enumerate() {
        for (i, (hand, unknown_count)) in hands.iter_mut().zip(unknown_counts.iter_mut()).enumerate() {
            // the current hand finished one round ago
            if finishing_index.map_or(false, |fi| fi == i) {
                break 'outer;
            }

            hand.apply_best_action(Card::random(), finishing_score.unwrap_or(f64::INFINITY), min_unknown_count);
            *unknown_count = hand.count_unknowns();
            min_unknown_count = min_unknown_count.min(*unknown_count);

            // first hand that finishes
            if hand.finished() && finishing_index.is_none() {
                finishing_index = Some(i);
                finishing_score = hand.score();  // returns a option but it will be some as the hand finished
            }
        }
    }

    let finishing_score = finishing_score.unwrap();
    let finishing_index = finishing_index.unwrap();

    let mut scores: Vec<f64> = hands.iter().map(|hand| hand.expected_score()).collect();
    if scores.iter().enumerate().any(|(i, score)| i != finishing_index && *score <= finishing_score) {
        scores[finishing_index] *= 2.0;
    }
    let best_score = *scores.iter().min_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal)).unwrap();
    let winner = scores.iter().position(|score| *score == best_score).unwrap();

    (winner, round, best_score)
}


fn compare_n(lookups: Vec<Lookup>, n: usize, style: ProgressStyle) -> Vec<Vec<(usize, f64)>> {
    let progbar = ProgressBar::new(n as u64).with_style(style);
    let num_lookups = lookups.len();
    let lookups = [&lookups[..], &lookups[..]].concat();  // double up so that the starting hand rotates its lookup

    (0..n).into_par_iter().progress_with(progbar)
        .map(|i| compare(&lookups[(i % num_lookups)..(i % num_lookups + num_lookups)]))
        .fold(|| iter::repeat_with(|| Vec::<(usize, f64)>::new()).take(num_lookups).collect::<Vec<_>>(), |mut m, (winner, round, score)| {
            m[winner].push((round, score));
            m
        })
        .reduce(|| iter::repeat_with(|| Vec::<(usize, f64)>::new()).take(num_lookups).collect::<Vec<_>>(), |mut m, n| {
            for (mi, ni) in m.iter_mut().zip(n.into_iter()) {
                mi.extend(ni);
            }
            m
        })
}
