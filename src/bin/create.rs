#[macro_use]
extern crate clap;
extern crate indicatif;

use clap::{App, Arg};
use indicatif::ProgressStyle;
use skyjo::{Lookup, lookup_compute::{self as luc, LookupCompute}};
use std::{env, error::Error};


fn main() -> Result<(), Box<dyn Error>> {
    let progbar_style = ProgressStyle::default_bar()
            .progress_chars("=>.")
            .template("{percent:>3.0}% [{bar:60}] eta: {eta_precise} - {msg}");

    let matches = App::new("skyjo-strat-create")
            .version(crate_version!())
            .author("Christoph S <chrissidtmeier@gmail.com>")
            .about("Helps you win at Skyjo")
            .arg(Arg::with_name("name")
                .short("n")
                .long("name")
                .takes_value(true)
                .required(true)
                .help("the name of the lookup to-be-created"))
            .subcommand(luc::Best::subcommand())
            .subcommand(luc::Expected::subcommand())
            .subcommand(luc::GraphFlow::subcommand())
            .subcommand(luc::LastRoundExp::subcommand())
            .subcommand(luc::Merged::subcommand())
            .subcommand(luc::Persistent::subcommand())
            .subcommand(luc::ReverseExpect::subcommand())
            .subcommand(luc::REGF::subcommand())
            .subcommand(luc::Simulated::subcommand())
            .get_matches();

    // create
    let name = matches.value_of("name").unwrap();
    println!("Creating lookup {}", name);
    let lookup = match matches.subcommand() {
        ("best",          Some(matches)) => luc::Best::from_matches(matches)?.compute(progbar_style.clone()),
        ("expected",      Some(matches)) => luc::Expected::from_matches(matches)?.compute(progbar_style.clone()),
        ("graphflow",     Some(matches)) => luc::GraphFlow::from_matches(matches)?.compute(progbar_style.clone()),
        ("lastroundexp",  Some(matches)) => luc::LastRoundExp::from_matches(matches)?.compute(progbar_style.clone()),
        ("merged",        Some(matches)) => luc::Merged::from_matches(matches)?.compute(progbar_style.clone()),
        ("persistent",    Some(matches)) => luc::Persistent::from_matches(matches)?.compute(progbar_style.clone()),
        ("reverseexpect", Some(matches)) => luc::ReverseExpect::from_matches(matches)?.compute(progbar_style.clone()),
        ("regf",          Some(matches)) => luc::REGF::from_matches(matches)?.compute(progbar_style.clone()),
        ("simulated",     Some(matches)) => luc::Simulated::from_matches(matches)?.compute(progbar_style.clone()),
        (s, _) => {
            return Err(Box::new(clap::Error::with_description(&format!("Invalid subcommand '{}'", s), clap::ErrorKind::InvalidSubcommand)));
        },
    };

    // write
    println!("Writing lookup to {}", Lookup::filename(name));
    lookup.save_as(&name)?;

    Ok(())
}
