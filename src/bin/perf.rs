#[macro_use]
extern crate clap;
extern crate csv;
extern crate indicatif;
extern crate rayon;

use clap::{App, Arg};
use csv::WriterBuilder;
use indicatif::{ProgressBar, ProgressStyle, ParallelProgressIterator};
use rayon::prelude::*;
use skyjo::{Card, Hand, Lookup};
use std::{env, cmp::Ordering, collections::HashMap, error::Error};


const MAX_ROUNDS: usize = 60;

const BEAT_N: usize = 1_500;
const FINISHED_N: usize = 100_000;
const TIME_N: usize = 100_000;

const START_SCORE: f64 = 40.0;
const END_SCORE: f64 = -20.0;
const SCORE_STEP: f64 = 1.0;


fn main() -> Result<(), Box<dyn Error>> {
    // TODO split in classes, with Perf trait: run_once, run, write_to, filename (like in lookup), ...

    let progbar_style = ProgressStyle::default_bar()
            .progress_chars("=>.")
            .template("{percent:>3.0}% [{bar:60}] eta: {eta_precise} - {msg}");

    let matches = App::new("skyjo-strat-perf")
            .version(crate_version!())
            .author("Christoph S <chrissidtmeier@gmail.com>")
            .about("Helps you win at Skyjo")
            .arg(Arg::with_name("name")
                .short("n")
                .long("name")
                .takes_value(true)
                .required(true)
                .help("the lookup to test"))
            .arg(Arg::with_name("skip-beat")
                .long("skip-beat")
                .short("B")
                .takes_value(false)
                .help("whether or not to skip the score_beaten_after test"))
            .arg(Arg::with_name("skip-finished")
                .long("skip-finished")
                .short("F")
                .takes_value(false)
                .help("whether or not to skip the finished_after test"))
            .arg(Arg::with_name("skip-time")
                .long("skip-time")
                .short("T")
                .takes_value(false)
                .help("whether or not to skip the score_over_time test"))
            .get_matches();

    // load
    let name = matches.value_of("name").unwrap();
    let lookup = Lookup::from_name(&name)?;
    println!("Read lookup from {}", Lookup::filename(name));

    // config
    let run_finished = !matches.is_present("skip-finished");
    let run_beat = !matches.is_present("skip-beat");
    let run_time = !matches.is_present("skip-time");


    // test beat
    if run_beat {
        let filename = format!("{}/{}.beat.csv", Lookup::RESULT_DIR, name);
        println!("Testing lookup {} [score_beaten_after]", filename);
        write_results(&filename,
                      score_beaten_after(&lookup, BEAT_N, progbar_style.clone(), START_SCORE, END_SCORE, SCORE_STEP, MAX_ROUNDS),
                      BEAT_N)?;
    }

    // test finished
    if run_finished {
        let filename = format!("{}/{}.finished.csv", Lookup::RESULT_DIR, name);
        println!("Testing lookup {} [finished_after]", filename);
        write_results(&filename,
                      finished_after(&lookup, FINISHED_N, progbar_style.clone(), MAX_ROUNDS),
                      FINISHED_N)?;
    }

    // test time
    if run_time {
        let filename = format!("{}/{}.time.csv", Lookup::RESULT_DIR, name);
        println!("Testing lookup {} [score_over_time]", filename);
        write_results(&filename,
                      score_over_time(&lookup, TIME_N, progbar_style.clone(), MAX_ROUNDS),
                      TIME_N)?;
    }

    Ok(())
}


fn rounds_to_beat(test_lookup: &Lookup, score_to_beat: f64, give_up_after: usize) -> Option<usize> {
    let mut hand = Hand::<'_>::unknown(test_lookup);

    for i in 0..give_up_after {
        hand.apply_best_action(Card::random(), score_to_beat, give_up_after - i);
        if let Some(score) = hand.score() {
            return if score < score_to_beat || 2.0 * score <= score_to_beat { Some(i) } else { None };
        }
    }
    None
}


fn score_beaten_after(test_lookup: &Lookup, n: usize, style: ProgressStyle, start_score: f64, end_score: f64, step: f64, give_up_after: usize) -> Vec<(f64, Vec<f64>)> {
    let progbar = ProgressBar::new(((start_score - end_score) / step) as u64).with_style(style);

    let mut ret: Vec<(f64, Vec<f64>)> = Vec::new();
    let mut score_to_beat = start_score;

    loop {
        progbar.set_message(&format!("current score to beat: {:.1}", score_to_beat));

        ret.push((score_to_beat, (0..n).into_par_iter()
            .filter_map(|_| rounds_to_beat(test_lookup, score_to_beat, give_up_after))  // TODO, dont exclude long runs
            .map(|r| r as f64)
            .collect()));
        score_to_beat -= step;

        progbar.inc(1);


        // no do-while :(
        if ret.last().unwrap().1.len() == 0 || score_to_beat <= end_score {
            break;
        }
    }

    progbar.finish_and_clear();

    ret
}


fn play_until_finish(test_lookup: &Lookup, give_up_after: usize) -> Option<(usize, f64)> {
    let mut hand = Hand::<'_>::unknown(test_lookup);

    for i in 0..give_up_after {
        hand.apply_best_action(Card::random(), f64::INFINITY, give_up_after - i);
        if let Some(score) = hand.score() {
            return Some((i, score));
        }
    }

    None
}


fn finished_after(test_lookup: &Lookup, n: usize, style: ProgressStyle, give_up_after: usize) -> Vec<(f64, Vec<f64>)> {
    let progbar = ProgressBar::new(n as u64).with_style(style);

    (0..n).into_par_iter().progress_with(progbar)
        .filter_map(|_| play_until_finish(test_lookup, give_up_after))
        .fold(|| HashMap::<usize, Vec<f64>>::new(), |mut m, (round, score)| {
            m.entry(round).or_insert_with(|| Vec::new()).push(score);
            m
        })
        .reduce(|| HashMap::<usize, Vec<f64>>::new(), |mut m, n| {
            for (round, v) in n.into_iter() {
                m.entry(round).or_insert_with(|| Vec::new()).extend(v);
            }
            m
        })
        .into_par_iter()
        .map(|(round, v)| (round as f64, v))
        .collect()
}


fn score_curve(test_lookup: &Lookup, give_up_after: usize) -> Vec<f64> {
    let mut hand = Hand::<'_>::unknown(test_lookup);
    let mut scores = Vec::with_capacity(give_up_after / 3);

    let mut rounds_remaining = give_up_after;
    while rounds_remaining > 0 && !hand.finished() {
        hand.apply_best_action(Card::random(), f64::INFINITY, rounds_remaining);
        scores.push(hand.expected_score());
        rounds_remaining -= 1;
    }

    scores
}


fn score_over_time(test_lookup: &Lookup, n: usize, style: ProgressStyle, give_up_after: usize) -> Vec<(f64, Vec<f64>)> {
    let progbar = ProgressBar::new(n as u64).with_style(style);

    (0..n).into_par_iter().progress_with(progbar)
        .flat_map(|_| score_curve(test_lookup, give_up_after).into_par_iter().enumerate())
        .fold(|| HashMap::<usize, Vec<f64>>::new(), |mut m, (round, score)| {
            m.entry(round).or_insert_with(|| Vec::new()).push(score);
            m
        })
        .reduce(|| HashMap::<usize, Vec<f64>>::new(), |mut m, n| {
            for (round, v) in n.into_iter() {
                m.entry(round).or_insert_with(|| Vec::new()).extend(v);
            }
            m
        })
        .into_par_iter()
        .map(|(round, v)| (round as f64, v))
        .collect()
}


fn write_results(filename: &str, results: Vec<(f64, Vec<f64>)>, n: usize) -> Result<(), Box<dyn Error>> {
    // create writer
    let mut wtr = WriterBuilder::new()
        .flexible(false)
        .has_headers(false)  // no automatic headers
        .from_path(filename)?;

    // write headers
    wtr.serialize(("x", "mean", "stdev", "median", "proportion"))?;

    results.into_iter()
        .map(|(x, ys)| {
            let (mean, stdev) = mean_stdev(&ys);
            let median = median(&ys);
            wtr.serialize((x, mean, stdev, median, ys.len() as f64 / n as f64))
        })
        .collect::<Result<Vec<()>, csv::Error>>()?;

    wtr.flush()?;
    Ok(())
}


fn mean(x: &[f64]) -> f64 {
    if x.len() == 0 { 0.0 } else { x.iter().sum::<f64>() / x.len() as f64 }
}

fn stdev(m: f64, x: &[f64]) -> f64 {
    mean(&x.iter().map(|a| (a - m).powi(2)).collect::<Vec<_>>()).sqrt()
}

fn mean_stdev(x: &[f64]) -> (f64, f64) {
    let m = mean(x);
    let s = stdev(m, x);
    (m, s)
}

fn median(x: &[f64]) -> f64 {
    if x.len() == 0 {
        0.0
    }
    else {
        let mut v = x.to_vec();
        v.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal));
        let len = v.len();

        if len % 2 == 0 {
            (v[len / 2] + v[len / 2 - 1]) / 2.0
        }
        else {
            v[len / 2]
        }
    }
}
