extern crate rand;

use std::{iter, fmt::Write};
use rand::{thread_rng, seq::SliceRandom};


#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy)]
pub enum Card {
    Value(i8),
    Unknown,
}


impl Card {
    pub const NUM_CARDS: usize = 15;
    pub const VALUES: [i8; Card::NUM_CARDS] = [
        -2, -1, 0, 1, 2,
        3, 4, 5, 6, 7,
        8, 9, 10, 11, 12];
    pub const WEIGHTS: [usize; Card::NUM_CARDS] = [
        1, 2, 3, 2, 2,
        2, 2, 2, 2, 2,
        2, 2, 2, 2, 2];
    pub const WEIGHT_SUM: usize = 30;

    pub const HEXDIGITS: [char; 16] = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];


    pub fn new(n: i8) -> Result<Card, String> {
        if Card::VALUES.contains(&n) {
            Ok(Card::Value(n))
        }
        else {
            Err(format!("Invalid card value {}", n))
        }
    }

    pub fn random() -> Card {
        Card::Value(*Card::VALUES.choose(&mut thread_rng()).unwrap())
    }

    pub fn from_hex(hex: char) -> Result<Card, String> {
        let x = Card::HEXDIGITS.iter().position(|&c| c == hex).ok_or(format!("Unknown character {}", hex))?;
        if x == 0 {
            Ok(Card::Unknown)
        }
        else {
            Ok(Card::Value(Card::VALUES[x - 1]))
        }
    }

    pub fn to_hex(&self) -> char {
        let i = match self {
            Card::Unknown => 0,
            Card::Value(v) => Card::VALUES.iter().position(|&x| x == *v).unwrap() + 1,
        };

        let mut s = String::with_capacity(1);
        write!(&mut s, "{:X}", i).unwrap();
        s.pop().unwrap()
    }

    pub fn iter_with_unknown() -> impl iter::Iterator<Item=Card> {
        iter::once(Card::Unknown).chain(Card::iter_values())
    }

    pub fn iter_values() -> impl iter::Iterator<Item=Card> {
        Card::VALUES.iter().map(|v| Card::Value(*v))
    }

    pub fn iter_weights() -> impl iter::Iterator<Item=(Card, usize)> {
        Card::iter_values().zip(&Card::WEIGHTS).map(|(card, weight)| (card, *weight))
    }

    pub fn iter_probs() -> impl iter::Iterator<Item=(Card, f64)> {
        Card::iter_weights().map(|(card, weight)| (card, weight as f64 / Card::WEIGHT_SUM as f64))
    }

    pub fn is_known(&self) -> bool {
        match self {
            Card::Value(_) => true,
            Card::Unknown => false,
        }
    }

    pub fn value(&self) -> Option<i8>  {
        match self {
            Card::Value(n) => Some(*n),
            Card::Unknown => None,
        }
    }

    pub fn index(&self) -> Option<usize>  {
        match self {
            Card::Value(n) => Card::VALUES.iter().position(|v| v == n),  // will be Some
            Card::Unknown => None,
        }
    }

    pub fn weight(&self) -> Option<usize>  {
        self.index().map(|i| Card::WEIGHTS[i])
    }

    pub fn prob(&self) -> Option<f64>  {
        self.weight().map(|w| w as f64 / Card::WEIGHT_SUM as f64)
    }

    pub fn weighted_average<F: FnMut(Card) -> f64>(mut f: F) -> f64 {
        Card::iter_probs().fold::<f64, _>(0.0, |avg, (card, prob)| avg + f(card) * prob)
    }

    pub fn try_weighted_average<E, F>(mut f: F) -> Result<f64, E>
    where F: FnMut(Card) -> Result<f64, E> {
        Card::iter_probs().try_fold::<f64, _, Result<f64, E>>(0.0, |avg, (card, prob)| Ok(avg + f(card)? * prob))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic(expected = "Invalid card value")]
    fn card_value_too_low() {
        Card::new(-3).unwrap();
    }

    #[test]
    #[should_panic(expected = "Invalid card value")]
    fn card_value_too_high() {
        Card::new(13).unwrap();
    }

    #[test]
    fn props_sum_to_one() {
        assert_eq!(Card::WEIGHT_SUM, Card::WEIGHTS.iter().sum());
    }

    #[test]
    fn fifteen_cards() {
        // necessary for the hex shenanigans to work
        assert_eq!(Card::VALUES.len(), 15);
    }

    #[test]
    fn test_from_hex() {
        assert_eq!(Card::Unknown, Card::from_hex('0').unwrap());
        assert_eq!(Card::Value(-2), Card::from_hex('1').unwrap());
        assert_eq!(Card::Value(5), Card::from_hex('8').unwrap());
        assert_eq!(Card::Value(12), Card::from_hex('F').unwrap());
    }
}
