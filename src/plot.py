from plotly import graph_objs as go
from plotly.subplots import make_subplots
from collections import Counter
import os


RESULT_DIR = "results"

COLORS = [(0x00, 0x00, 0x00),
          (0x8B, 0x45, 0x13),
          (0x19, 0x19, 0x70),
          (0x00, 0x64, 0x00),
          (0x00, 0x80, 0x80),
          (0xFF, 0x00, 0x00),
          (0xFF, 0xA5, 0x00),
          (0xFF, 0xFF, 0x00),
          (0x00, 0x00, 0xCD),
          (0x00, 0xFF, 0x00),
          (0xFF, 0x00, 0xFF),
          (0xDD, 0xA0, 0xDD),
          (0xFF, 0x14, 0x93),
          (0x98, 0xFB, 0x98),
          (0xFF, 0xDE, 0xAD)]


def csv_to_dict(filename):
    with open(filename) as f:
        lines = f.readlines()

    rows = [line.strip().split(",") for line in lines]

    header, *rows = rows
    rows = [list(map(float, row)) for row in rows]
    i = header.index("x")
    rows = sorted(rows, key=lambda row: row[i])

    cols = list(zip(header, *rows))

    return {head: values for head, *values in cols}


class LookupData():
    def __init__(self, name):
        self.name = name
        self.beat = csv_to_dict(os.path.join(RESULT_DIR, name + ".beat.csv"))
        self.finished = csv_to_dict(os.path.join(RESULT_DIR, name + ".finished.csv"))
        self.time = csv_to_dict(os.path.join(RESULT_DIR, name + ".time.csv"))
        self.cmp = csv_to_dict(os.path.join(RESULT_DIR, name + ".cmp.csv"))


class Plot():
    def __init__(self):
        self.fig = make_subplots(
                rows=2, cols=4,
                shared_xaxes=True,
                subplot_titles=("Score to beat", "Finished after", "Score over time", "Compare with expected"),
                horizontal_spacing=0.06,
                vertical_spacing=0.04)
        self.data = {}

    def add_trace(self, lookup_data, name):
        self.data[name] = lookup_data
        self.add_to_fig(lookup_data.beat, name, COLORS[len(self.data) % len(COLORS)], 1, xlabel="Score", ylabels=["Rounds", "Proportion"])
        self.add_to_fig(lookup_data.finished, name, COLORS[len(self.data) % len(COLORS)], 2, xlabel="Rounds", ylabels=["Score", "Proportion"])
        self.add_to_fig(lookup_data.time, name, COLORS[len(self.data) % len(COLORS)], 3, xlabel="Rounds", ylabels=["Score", "Proportion"])
        self.add_to_fig(lookup_data.cmp, name, COLORS[len(self.data) % len(COLORS)], 4, xlabel="Rounds", ylabels=["Score", "Proportion"])

    def add_to_fig(self, data_dict, name, color, col, xlabel="", ylabels=("", "")):
        full = "rgb" + str(color)
        trans = "rgba" + str((*color, .1))

        # score plot
        self.fig.add_trace(
            go.Scatter(
                x=data_dict["x"],
                y=data_dict["median"],

                mode="lines",
                line_color=full,

                name=name,
                showlegend=(col == 1),
                legendgroup=name,
            ),
            row=1,
            col=col,
        )
        self.fig.add_trace(
            go.Scatter(
                x=data_dict["x"],
                y=data_dict["mean"],

                mode="lines",
                line_color=full,
                line_dash="dash",

                hoverinfo="skip",
                showlegend=False,
                legendgroup=name,
            ),
            row=1,
            col=col,
        )

        # error bar
        upper = [m + s for (m, s) in zip(data_dict["mean"], data_dict["stdev"])]
        lower = [m - s for (m, s) in zip(data_dict["mean"], data_dict["stdev"])]
        self.fig.add_trace(
            go.Scatter(
                x=data_dict["x"] + data_dict["x"][::-1],  # x, then x reversed
                y=upper + lower[::-1],  # upper, then lower reversed

                mode="none",

                fill="toself",
                fillcolor=trans,

                hoverinfo="skip",
                showlegend=False,
                legendgroup=name,
            ),
            row=1,
            col=col,
        )

        # peak dot
        average_mean = sum(p * a for p, a in zip(data_dict["proportion"], data_dict["mean"])) / sum(data_dict["proportion"])
        average_x = sum(p * a for p, a in zip(data_dict["proportion"], data_dict["x"])) / sum(data_dict["proportion"])
        self.fig.add_trace(
            go.Scatter(
                x=[average_x],
                y=[average_mean],

                mode="markers",
                line_color=full,

                hoverinfo="skip",
                showlegend=False,
                legendgroup=name,
                name=name,
            ),
            row=1,
            col=col,
        )

        # props curve
        self.fig.add_trace(
            go.Scatter(
                x=data_dict["x"],
                y=data_dict["proportion"],

                mode="lines",
                line_color=full,

                name=name,
                showlegend=False,
                legendgroup=name,
            ),
            row=2,
            col=col,
        )

        # axes titles
        self.fig.update_xaxes(title_text=xlabel, row=2, col=col)
        for i, ylabel in enumerate(ylabels):
            self.fig.update_yaxes(title_text=ylabel, row=i + 1, col=col)

    def show(self):
        self.fig.show()


if __name__ == "__main__":
    plot = Plot()

    names = [filename.split(".")[0] for filename in os.listdir(RESULT_DIR) if filename.endswith(".csv")]
    c = Counter(names)
    names = [name for name in c.keys() if c[name] == 4]

    for name in sorted(names):
        plot.add_trace(LookupData(name), name)
    plot.show()
