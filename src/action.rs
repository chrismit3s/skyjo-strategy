use crate::Card;


#[derive(Debug, Clone, Copy)]
pub struct CardReplace {
    pub col_index: usize,
    pub score_delta: f64,
    pub card: Card
}

impl CardReplace {
    pub fn to_draw_or_replace(self) -> DrawOrReplace {
        DrawOrReplace::Replace(self)
    }

    pub fn to_flip_or_replace(self) -> FlipOrReplace {
        FlipOrReplace::Replace(self)
    }
}


#[derive(Debug, Clone, Copy)]
pub struct CardFlip {
    pub col_index: usize,
    pub score_delta: f64,
}

impl CardFlip {
    pub fn to_flip_or_replace(self) -> FlipOrReplace {
        FlipOrReplace::Flip(self)
    }
}


#[derive(Debug, Clone, Copy)]
pub enum FlipOrReplace {
    Replace(CardReplace),
    Flip(CardFlip),
}

impl FlipOrReplace {
    pub fn replace(col_index: usize, score_delta: f64, from: Card) -> FlipOrReplace {
        FlipOrReplace::Replace(CardReplace { col_index, score_delta, card: from })
    }

    pub fn flip(col_index: usize, score_delta: f64) -> FlipOrReplace {
        FlipOrReplace::Flip(CardFlip { col_index, score_delta })
    }

    pub fn use_best(self, other: Self) -> Self {
        if self.score_delta() < other.score_delta() {
            self
        }
        else {
            other
        }
    }

    pub fn score_delta(&self) -> f64 {
        match self {
            FlipOrReplace::Replace(CardReplace { col_index: _, score_delta, card: _ }) => *score_delta,
            FlipOrReplace::Flip(CardFlip { col_index: _, score_delta }) => *score_delta,
        }
    }

    pub fn col_index(&self) -> usize {
        match self {
            FlipOrReplace::Replace(CardReplace { col_index, score_delta: _, card: _ }) => *col_index,
            FlipOrReplace::Flip(CardFlip { col_index, score_delta: _ }) => *col_index,
        }
    }
}


#[derive(Debug, Clone, Copy)]
pub enum DrawOrReplace {
    Replace(CardReplace),
    Draw([FlipOrReplace; Card::NUM_CARDS]),
}

impl DrawOrReplace {
    pub fn replace(col_index: usize, score_delta: f64, from: Card) -> DrawOrReplace {
        DrawOrReplace::Replace(CardReplace { col_index, score_delta, card: from })
    }

    pub fn draw(actions: [FlipOrReplace; Card::NUM_CARDS]) -> DrawOrReplace {
        DrawOrReplace::Draw(actions)
    }

    pub fn use_best(self, other: Self) -> Self {
        if self.score_delta() < other.score_delta() {
            self
        }
        else {
            other
        }
    }

    pub fn score_delta(&self) -> f64 {
        match self {
            DrawOrReplace::Replace(CardReplace { col_index: _, score_delta, card: _ }) => *score_delta,
            DrawOrReplace::Draw(actions) => Card::weighted_average(|card| actions[card.index().unwrap()].score_delta()),
        }
    }
}
