extern crate cached;

use cached::{proc_macro::cached, SizedCache};
use crate::{Card, Lookup};
use std::{fmt, iter};


/// computes nCr(n + r - 1, r) = (n + r - 1)! / (r! * (n - 1)!)
#[cached(size=1)]
fn len_comb_with_rep(n: usize, r: usize) -> usize {
    // we compute (n + r - 1) * (n + r - 2) * ... * n / 1 * 2 * ... * r
    let mut result = n;

    let mut denom_factor = 2;  // goes from 2 to at most r
    for num_factor in (n + 1)..(n + r) { // goes from n + 1 to n + r (n is already stored in result)
        result *= num_factor;
        if result % denom_factor == 0 {
            result /= denom_factor;
            denom_factor += 1;
        }
    }

    for denom_factor in denom_factor..(r + 1) {  // goes over the remaining factors of the denominator
        result /= denom_factor;
    }

    result
}

/// computes all combinations of the set l with length r with repetition allowed
#[cached(size=1)]
fn comb_with_rep(l: &'static [char], r: usize) -> Vec<Vec<char>> {
    // Reference python code:
    //
    // def gen(l, r):
    //     if r <= 0:
    //         yield []
    //         return
    //
    //     for i, c in enumerate(l):
    //         for x in gen(l[i:], r - 1):
    //             x.insert(0, c)
    //             yield x

    if r == 0 {
        vec![vec![]]
    }
    else if r == 1 {
        l.iter().map(|c| vec![*c]).collect()
    }
    else {
        l.iter()
            .enumerate()
            .flat_map(|(i, x)| comb_with_rep(&l[i..], r - 1).into_iter().map(move |mut v| { v.insert(0, *x); v }))
            .collect()

    }
}

#[cached(
    type = "SizedCache<[usize; Card::NUM_CARDS], f64>",
    create = "{ SizedCache::with_size(816) }",  // 816 is the number of possible cols 15 cards and 3 cards per col
    convert = r#"{ col.counts.clone() }"#
)]
fn _expected_score_rec(col: &mut Col) -> f64 {
    if let Some(score) = col.score() {
        // column is known
        score
    }
    else {
        // flip one unknown card and compute the average score of that column
        Card::weighted_average(|card| {
            // do
            col.flip_to(card).unwrap();

            // average score after flipping a card
            let score = _expected_score_rec(col);

            // undo
            col.replace_with(card, Card::Unknown).unwrap();

            score
        })
    }
}


#[derive(Eq, Clone, Copy, Debug, Hash)]
pub struct Col {
    counts: [usize; Card::NUM_CARDS],
}


impl Col {
    pub const SIZE: usize = 3;

    pub fn iter_len() -> usize {
        len_comb_with_rep(Card::HEXDIGITS.len(), Col::SIZE)
    }

    pub fn iter() -> impl iter::Iterator<Item=Col> {
        comb_with_rep(&Card::HEXDIGITS, Col::SIZE).into_iter()
            .map(|v| Col::from_hex(&v.into_iter().collect::<String>()).unwrap())
    }

    pub fn unknown() -> Col {
        let counts: [usize; Card::NUM_CARDS] = [0; Card::NUM_CARDS];
        Col { counts }
    }

    pub fn from_cards(cards: &[Card]) -> Result<Col, String> {
        if cards.len() > Col::SIZE {
            Err("Too many cards for one column".to_string())
        }
        else {
            let mut col = Col::unknown();
            for c in cards.iter().filter(|card| card.is_known()) {
                col.counts[c.index().unwrap()] += 1;
            }

            Ok(col)
        }
    }

    pub fn from_hex<'a>(hex: &str) -> Result<Col, String> {
        let cards = hex.chars().map(|c| Card::from_hex(c)).collect::<Result<Vec<_>, String>>()?;
        Col::from_cards(&cards[..])
    }

    pub fn to_hex(&self) -> String {
        self.iter_all().map(|card| card.to_hex()).collect::<String>()
    }

    pub fn score(&self) -> Option<f64> {
        if self.has_unknowns() {
            None
        }
        else if self.is_triple() {
            Some(0.0)
        }
        else {
            Some(self.iter_all().map(|c| c.value().unwrap() as f64).sum())
        }
    }

    pub fn expected_score(&self) -> f64 {
        _expected_score_rec(&mut self.clone())
    }

    pub fn count(&self, card: Card) -> usize {
        match card {
            Card::Value(_) => self.counts[card.index().unwrap()],
            Card::Unknown => {
                let sum: usize = self.counts.iter().sum();
                if sum > Col::SIZE {
                    panic!("Column with {} known cards encountered ({:?}), Col::SIZE is {}", sum, self.counts, Col::SIZE);
                }

                // this is safe now because of the check above
                (Col::SIZE as isize - sum as isize) as usize
            }
        }
    }

    /// iterates over all cards in this columns, with repetition and Unknowns
    pub fn iter_all<'a>(&'a self) -> impl iter::Iterator<Item=Card> + 'a {
        Card::iter_with_unknown().flat_map(move |card| iter::repeat(card).take(self.count(card)))
    }

    /// iterates over different types of cards in this columns, with Unknown
    pub fn iter_all_different<'a>(&'a self) -> impl iter::Iterator<Item=Card> + 'a {
        Card::iter_with_unknown().filter(move |&card| self.count(card) > 0)
    }

    /// iterates over different types of cards in this columns, without Unknown
    pub fn iter_different<'a>(&'a self) -> impl iter::Iterator<Item=Card> + 'a {
        Card::iter_values().filter(move |&card| self.count(card) > 0)
    }

    pub fn is_triple(&self) -> bool {
        self.counts.iter()
                .map(|&count| count)  // maps reference to actual value
                .filter(|&count| count > 0)  // filter again only gets a reference
                .nth(0)
                .unwrap_or(0) == Col::SIZE
    }

    pub fn has_unknowns(&self) -> bool {
        self.count(Card::Unknown) != 0
    }

    pub fn replace_with(&mut self, from: Card, to: Card) -> Result<(), String> {
        if self.count(from) == 0 {
            return Err(format!("Card {:?} not found in column", from));
        }

        if from.is_known() {
            self.counts[from.index().unwrap()] -= 1;
        }

        if to.is_known() {
            self.counts[to.index().unwrap()] += 1;
        }

        Ok(())
    }

    pub fn after_replace_with(&self, from: Card, to: Card) -> Result<Self, String> {
        let mut clone = self.clone();
        clone.replace_with(from, to)?;
        Ok(clone)
    }

    pub fn all_after_replace(&self, to: Card) -> Result<Vec<Self>, String> {
        self.iter_all_different().map(|card| self.after_replace_with(card, to)).collect()
    }

    pub fn flip_to(&mut self, to: Card) -> Result<(), String> {
        self.replace_with(Card::Unknown, to)
    }

    pub fn after_flip_to(&self, to: Card) -> Result<Self, String> {
        let mut clone = self.clone();
        clone.flip_to(to)?;
        Ok(clone)
    }

    pub fn score_after_replace(&mut self, from: Card, to: Card, lookup: &Lookup, min_rounds: usize) -> Result<f64, String> {
        // do
        self.replace_with(from, to)?;

        let score = lookup.col_score(self, min_rounds);

        // undo
        self.replace_with(to, from).unwrap();

        Ok(score)
    }

    pub fn score_after_flip(&mut self, lookup: &Lookup, min_rounds: usize) -> Result<f64, String> {
        // average score after flipping a card
        Card::try_weighted_average(|card| {
            // do
            self.flip_to(card)?;

            let score = lookup.col_score(self, min_rounds);

            // undo
            self.replace_with(card, Card::Unknown).unwrap();

            Ok(score)
        })
    }

    pub fn best_score_after_replace(&mut self, to: Card, lookup: &Lookup, min_rounds: usize) -> (Card, f64) {
        let mut best_score = f64::INFINITY;
        let mut best_from = Card::Unknown;

        for from in self.clone().iter_all_different() {
            let curr_score = self.score_after_replace(from, to, lookup, min_rounds).unwrap();
            if best_score > curr_score {
                best_score = curr_score;
                best_from = from;
            }
        }

        (best_from, best_score)
    }
}


impl PartialEq for Col {
    fn eq(&self, other: &Self) -> bool {
        for c in Card::iter_with_unknown() {
            if self.count(c) != other.count(c) {
                return false;
            }
        }

        return true;
    }
}


impl fmt::Display for Col {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut out = String::new();

        for c in Card::iter_with_unknown() {
            out += &match c {
                Card::Value(v) => format!("{:2}|", v),
                Card::Unknown => " ?|".to_string(),
            }.repeat(self.count(c) as usize);
        }

        out.pop();
        write!(f, "{}", out)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    fn col_size(col: &Col) -> usize {
        col.counts.iter().sum::<usize>().max(Col::SIZE)
    }

    #[test]
    fn test_len_comb_with_rep() {
        for i in 1..10 {
            assert_eq!(i, len_comb_with_rep(i, 1));
        }

        // some precomputed values
        assert_eq!(816, len_comb_with_rep(16, 3));
        assert_eq!(5050, len_comb_with_rep(100, 2));
        assert_eq!(14950, len_comb_with_rep(23, 4));
        assert_eq!(490314, len_comb_with_rep(16, 8));
    }

    #[test]
    fn test_comb_with_rep_len() {
        for l in 1..16 {
            for r in 1..5 {
                let combs = comb_with_rep(&Card::HEXDIGITS[..l], r);
                assert_eq!(combs.len(), len_comb_with_rep(l as usize, r));
            }
        }
    }

    #[test]
    #[should_panic(expected = "Too many cards for one column")]
    fn too_many_cards() {
        Col::from_cards(&[Card::Value(-2), Card::Value(-1), Card::Value(0), Card::Value(1)]).unwrap();
    }

    #[test]
    fn replace_keeps_col_size() {
        let mut col = Col::from_cards(&[Card::Value(12), Card::Value(0), Card::Value(0)]).unwrap();

        col.replace_with(Card::Value(0), Card::Value(12)).unwrap();
        assert_eq!(col_size(&col), Col::SIZE);

        col.replace_with(Card::Value(12), Card::Value(1)).unwrap();
        assert_eq!(col_size(&col), Col::SIZE);

        col.replace_with(Card::Value(12), Card::Value(-1)).unwrap();
        assert_eq!(col_size(&col), Col::SIZE);
    }

    #[test]
    fn flip_keeps_col_size() {
        let mut col = Col::unknown();

        col.flip_to(Card::Value(12)).unwrap();
        assert_eq!(col_size(&col), Col::SIZE);

        col.flip_to(Card::Value(4)).unwrap();
        assert_eq!(col_size(&col), Col::SIZE);

        col.flip_to(Card::Value(-1)).unwrap();
        assert_eq!(col_size(&col), Col::SIZE);
    }

    #[test]
    #[should_panic(expected = "not found in column")]
    fn replace_bad_card() {
        let mut col = Col::from_cards(&[Card::Value(11), Card::Value(11)]).unwrap();
        col.replace_with(Card::Value(10), Card::Value(7)).unwrap();
    }

    #[test]
    #[should_panic(expected = "not found in column")]
    fn flip_in_full_col() {
        let mut col = Col::from_cards(&[Card::Value(11), Card::Value(11), Card::Value(11)]).unwrap();
        col.flip_to(Card::Value(7)).unwrap();
    }

    #[test]
    fn col_size_three() {
        // needed for _expected_score_rec as its made to take three args, its easy to adapt to other
        // sizes, but hard to do so generally
        assert_eq!(Col::SIZE, 3);
    }
}
