use crate::{Col, Hand};
use std::{error::Error, io::{Write, Read}, fs::File, collections::HashMap};


fn parse_line(line: &str) -> Result<(Col, Vec<f64>), String> {
    let fields: Vec<_> = line.split(',').collect();

    let col_hexstr = fields[0];
    let col = Col::from_hex(&col_hexstr).map_err(|_| format!("failed to parse the column {:?}", col_hexstr))?;

    let scores: Vec<f64> = fields[1..]
        .iter()
        .map(|score_str| score_str.parse::<f64>().map_err(|_| format!("failed to parse the float {:?}", score_str)))
        .collect::<Result<_, _>>()?;

    Ok((col, scores))
}


fn format_line(col: &Col, scores: &[f64]) -> String {
    let col_hexstr = col.to_hex();
    let scores_str = scores.iter().map(f64::to_string).collect::<Vec<_>>().join(",");
    format!("{},{}\n", col_hexstr, scores_str)
}


#[derive(Clone)]
pub struct Lookup {
    pub lookup: HashMap<Col, Vec<f64>>,
}


impl Lookup {
    pub const LOOKUP_DIR: &'static str = "lookups";
    pub const RESULT_DIR: &'static str = "results";

    pub fn filename(name: &str) -> String {
        format!("{}/{}.lkp", Lookup::LOOKUP_DIR, name)
    }

    /// a simple lookup only contains one score per column, so the score is independent
    /// from how many rounds there will be
    pub fn new_simple(lookup: HashMap<Col, f64>) -> Lookup {
        let lookup = lookup.into_iter().map(|(col, score)| (col, vec![score])).collect();
        Lookup { lookup }
    }

    /// a simple lookup only contains many scores per column, the score at index i is the
    /// score of the col when theres at least i rounds to be played
    pub fn new_complex(lookup: HashMap<Col, Vec<f64>>) -> Lookup {
        Lookup { lookup }
    }

    /// a simple lookup of the expected values
    pub fn expected() -> Lookup {
        let lookup: HashMap<Col, Vec<f64>> = Col::iter()
            .map(|col| (col.clone(), vec![col.expected_score()]))
            .collect();

        Lookup { lookup }
    }

    /// reads a lookup from a file in csv format
    pub fn from_file(filename: &str) -> Result<Lookup, String> {
        let mut file = File::open(filename).map_err(|_| format!("failed to open file {}", filename))?;

        let mut buf = String::new();
        file.read_to_string(&mut buf).map_err(|_| format!("failed to read file {}", filename))?;

        let lookup: HashMap<Col, Vec<f64>> = buf.split("\n")
                .map(|line| line.trim())
                .filter(|line| line.len() != 0 && line.chars().nth(0).unwrap() != '#')  // skip empty lines and comments
                .map(parse_line)
                .collect::<Result<_, _>>()?;

        Ok(Lookup { lookup })
    }

    /// reads a lookup from the file `./$LOOKUP_DIR/<name>.lkp` in csv format
    pub fn from_name(name: &str) -> Result<Lookup, String> {
        Lookup::from_file(&Lookup::filename(name))
    }

    /// writes lookup to filename in csv format, will overwrite filename
    pub fn write_to(&self, filename: &str) -> Result<(), Box<dyn Error>> {
        let mut file = File::create(filename)?;
        let buf: Vec<u8> = Col::iter()
                .flat_map(|col| format_line(&col, self.get_scores(&col)).into_bytes())
                .collect();
        file.write(&buf[..])?;
        Ok(())
    }

    /// writes lookup to the file `./$LOOKUP_DIR/<name>.lkp` in csv format, will overwrite it
    pub fn save_as(&self, name: &str) -> Result<(), Box<dyn Error>> {
        self.write_to(&Lookup::filename(name))
    }

    pub fn get_complex_map(&self) -> HashMap<Col, Vec<f64>> {
        self.lookup.clone()
    }

    pub fn get_simple_map(&self, min_rounds: usize) -> HashMap<Col, f64> {
        Col::iter()
            .map(|col| (col.clone(), self.col_score(&col, min_rounds)))
            .collect()
    }

    pub fn get_scores(&self, col: &Col) -> &[f64] {
        let l = self.lookup.len();
        if l != 816 {
            let mut v = self.lookup.keys().map(|col| col.to_hex()).collect::<Vec<_>>();
            v.sort();
            panic!("get {}, lookup contains keys {}", col.to_hex(), v.join(","));
        }

        self.lookup
            .get(col)
            .expect(&format!("no scores found for col {}", col.to_hex()))
    }

    pub fn get_scores_mut(&mut self, col: &Col) -> &mut Vec<f64> {
        self.lookup
            .get_mut(col)
            .expect(&format!("no scores found for col {}", col.to_hex()))
    }

    pub fn col_score(&self, col: &Col, min_rounds: usize) -> f64 {
        let scores = self.get_scores(col);
        let i = min_rounds.min(scores.len() - 1);
        scores[i]
    }

    pub fn hand_score(&self, hand: &Hand, min_rounds: usize) -> f64 {
        hand.cols.iter().map(|col| self.col_score(col, min_rounds)).sum()
    }

    pub fn delta(&self, other: &Self) -> f64 {
        Col::iter()
            .flat_map(|col| self.get_scores(&col).iter().zip(other.get_scores(&col).iter()).map(|(s, o)| s - o))
            .sum::<f64>() / Col::iter_len() as f64
    }
}
