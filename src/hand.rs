use crate::{Card, Col, Lookup, action};
use std::{fmt, hash};


#[derive(Clone)]
pub struct Hand<'a> {
    pub cols: Vec<Col>,
    pub lookup: &'a Lookup,
}


impl<'a> Hand<'a> {
    pub const SIZE: usize = 4;

    pub const MIN_SCORE: f64 = -20.0;
    pub const MAX_SCORE: f64 = 350.0;


    pub fn unknown(lookup: &'a Lookup) -> Hand<'a> {
        let cols: Vec<Col> = (0..Self::SIZE).map(|_| Col::unknown()).collect();

        Hand { cols, lookup }
    }

    pub fn new(cols: &[Col], lookup: &'a Lookup) -> Result<Hand<'a>, String> {
        if cols.len() > Self::SIZE {
            Err(format!("{} is too many cols for one hand", cols.len()))
        }
        else {
            let cols = cols.to_vec();
            Ok(Hand { cols, lookup })
        }
    }

    fn score_delta(score_before: f64, score_after: f64, score_to_beat: f64, final_round: bool) -> f64 {
        if final_round && score_after >= score_to_beat {
            2.0 * score_after - score_before
        }
        else {
            score_after - score_before
        }
    }

    pub fn finished(&self) -> bool {
        self.cols.len() == 0 || !self.cols.iter().any(|col| col.has_unknowns())
    }

    pub fn score(&self) -> Option<f64> {
        self.cols.iter().map(|col| col.score()).sum()
    }

    pub fn expected_score(&self) -> f64 {
        self.cols.iter().map(|col| col.expected_score()).sum()
    }

    pub fn count_unknowns(&self) -> usize {
        self.cols.iter().map(|col| col.count(Card::Unknown)).sum()
    }

    pub fn best_flip(&mut self, score_to_beat: f64, min_rounds: usize) -> Option<action::CardFlip> {
        let mut best_score_delta = f64::INFINITY;
        let mut best_col_index: Option<usize> = None;

        let min_rounds = min_rounds.min(self.count_unknowns() - 1);
        let hand_score = self.lookup.hand_score(self, min_rounds);

        for (col_index, col) in self.cols.iter_mut().enumerate().filter(|(_i, col)| col.has_unknowns()) {
            let col_score_after = col.score_after_flip(self.lookup, min_rounds).unwrap();
            let hand_score_after = hand_score - self.lookup.col_score(col, min_rounds) + col_score_after;

            // keep best move
            let score_delta = Self::score_delta(hand_score, hand_score_after, score_to_beat, min_rounds == 0);
            if best_score_delta > score_delta {
                best_score_delta = score_delta;
                best_col_index = Some(col_index);
            }
        }

        best_col_index.map(|col_index| action::CardFlip { col_index, score_delta: best_score_delta })
    }

    pub fn best_replace_for(&mut self, to: Card, score_to_beat: f64, min_rounds: usize) -> action::CardReplace {
        let mut best_score_delta = f64::INFINITY;
        let mut best_col_index: usize = 0;
        let mut best_from = Card::Unknown;

        let hand_score = self.lookup.hand_score(self, min_rounds);

        // first check all without replacing an unknown card, risking to finish the game, as those
        // will be checked later
        for (col_index, col) in self.cols.iter_mut().enumerate() {
            for from in col.clone().iter_different() {
                let col_score_after = col.score_after_replace(from, to, self.lookup, min_rounds).unwrap();
                let hand_score_after = hand_score - self.lookup.col_score(col, min_rounds) + col_score_after;

                // keep best move
                let score_delta = Self::score_delta(hand_score, hand_score_after, score_to_beat, min_rounds == 0);
                if best_score_delta > score_delta {
                    best_score_delta = score_delta;
                    best_col_index = col_index;
                    best_from = from;
                }
            }
        }

        // now we only check the columns with unknowns, as we will swap `to` with an unknown card
        // now
        let min_rounds = min_rounds.min(self.count_unknowns() - 1);
        let hand_score = self.lookup.hand_score(self, min_rounds);

        for (col_index, col) in self.cols.iter_mut().enumerate().filter(|(_i, col)| col.has_unknowns()) {
            let col_score_after = col.score_after_replace(Card::Unknown, to, self.lookup, min_rounds).unwrap();
            let hand_score_after = hand_score - self.lookup.col_score(col, min_rounds) + col_score_after;

            // keep best move
            let score_delta = Self::score_delta(hand_score, hand_score_after, score_to_beat, min_rounds == 0);
            if best_score_delta > score_delta {
                best_score_delta = score_delta;
                best_col_index = col_index;
                best_from = Card::Unknown;
            }
        }

        action::CardReplace { col_index: best_col_index, score_delta: best_score_delta, card: best_from }
    }

    pub fn best_flip_or_replace_for(&mut self, drawn_card: Card, score_to_beat: f64, min_rounds: usize) -> action::FlipOrReplace {
        let flip_option = self.best_flip(score_to_beat, min_rounds);
        let replace = self.best_replace_for(drawn_card, score_to_beat, min_rounds);
        match flip_option {
            Some(flip) => flip.to_flip_or_replace().use_best(replace.to_flip_or_replace()),
            None => replace.to_flip_or_replace(),
        }
    }

    pub fn best_draw_or_replace_for(&mut self, card: Card, score_to_beat: f64, min_rounds: usize) -> action::DrawOrReplace {
        // generate mapping of all possible drawn cards and corresponding moves
        let mut flip_or_replaces: [action::FlipOrReplace; Card::NUM_CARDS] = [action::FlipOrReplace::flip(0, 0.0); Card::NUM_CARDS];  // will all be overwritten
        for (index, drawn_card) in Card::iter_values().enumerate() {
            flip_or_replaces[index] = self.best_flip_or_replace_for(drawn_card, score_to_beat, min_rounds);
        }

        let draw = action::DrawOrReplace::draw(flip_or_replaces);
        let replace = self.best_replace_for(card, score_to_beat, min_rounds).to_draw_or_replace();

        replace.use_best(draw)
    }

    pub fn apply_flip_or_replace(&mut self, drawn_card: Card, ac: action::FlipOrReplace) {
        match ac {
            action::FlipOrReplace::Replace(action::CardReplace { col_index, score_delta: _, card }) => {
                let col = &mut self.cols[col_index];

                col.replace_with(card, drawn_card).unwrap();
                if col.is_triple() {
                    self.cols.remove(col_index);
                }
            },

            action::FlipOrReplace::Flip(action::CardFlip { col_index, score_delta: _ }) => {
                let col = &mut self.cols[col_index];

                col.flip_to(Card::random()).unwrap();
                if col.is_triple() {
                    self.cols.remove(col_index);
                }
            },
        }
    }

    pub fn apply_draw_or_replace(&mut self, center_card: Card, ac: &action::DrawOrReplace) -> action::FlipOrReplace {
        let (flip_or_replace, card) = match *ac {
            action::DrawOrReplace::Replace(ac) => (ac.to_flip_or_replace(), center_card),
            action::DrawOrReplace::Draw(actions) => {
                let card = Card::random();
                (actions[card.index().unwrap()], card)
            }
        };
        self.apply_flip_or_replace(card, flip_or_replace);
        flip_or_replace
    }

    pub fn apply_best_action(&mut self, center_card: Card, score_to_beat: f64, min_rounds: usize) -> action::FlipOrReplace {
        let action = self.best_draw_or_replace_for(center_card, score_to_beat, min_rounds);
        self.apply_draw_or_replace(center_card, &action)
    }
}


impl<'a> PartialEq for Hand<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.cols.len() == other.cols.len()
            && self.cols.iter()
                .zip(other.cols.iter())
                .all(|(self_col, other_col)| self_col == other_col)
    }
}


impl<'a> Eq for Hand<'a> {}


impl<'a> fmt::Display for Hand<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let out: String = self.cols.iter().map(|col| format!("{}\n", col)).collect();

        write!(f, "{}", out)
    }
}


impl<'a> hash::Hash for Hand<'a> {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.cols.hash(state);
    }
}
