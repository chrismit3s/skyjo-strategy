cargo run --bin create --release -- --name regf-bydelta-lnmin-expramp-n4 regf -w lnmin -p expramp-n4
cargo run --bin create --release -- --name regf-bydelta-lnmin-expramp-n2 regf -w lnmin -p expramp-n2
cargo run --bin create --release -- --name regf-bydelta-lnmin-linearramp regf -w lnmin -p linearramp
cargo run --bin create --release -- --name regf-bydelta-lnmin-expramp-p2 regf -w lnmin -p expramp-p2
cargo run --bin create --release -- --name regf-bydelta-lnmin-expramp-p4 regf -w lnmin -p expramp-p4

cargo run --bin create --release -- --name regf-bydelta-linearmin-expramp-n4 regf -w linearmin -p expramp-n4
cargo run --bin create --release -- --name regf-bydelta-linearmin-expramp-n2 regf -w linearmin -p expramp-n2
cargo run --bin create --release -- --name regf-bydelta-linearmin-linearramp regf -w linearmin -p linearramp
cargo run --bin create --release -- --name regf-bydelta-linearmin-expramp-p2 regf -w linearmin -p expramp-p2
cargo run --bin create --release -- --name regf-bydelta-linearmin-expramp-p4 regf -w linearmin -p expramp-p4

cargo run --bin create --release -- --name regf-bydelta-elumin-expramp-n4 regf -w elumin -p expramp-n4
cargo run --bin create --release -- --name regf-bydelta-elumin-expramp-n2 regf -w elumin -p expramp-n2
cargo run --bin create --release -- --name regf-bydelta-elumin-linearramp regf -w elumin -p linearramp
cargo run --bin create --release -- --name regf-bydelta-elumin-expramp-p2 regf -w elumin -p expramp-p2
cargo run --bin create --release -- --name regf-bydelta-elumin-expramp-p4 regf -w elumin -p expramp-p4

cargo run --bin create --release -- --name regf-bydelta-relumin-expramp-n4 regf -w relumin -p expramp-n4
cargo run --bin create --release -- --name regf-bydelta-relumin-expramp-n2 regf -w relumin -p expramp-n2
cargo run --bin create --release -- --name regf-bydelta-relumin-linearramp regf -w relumin -p linearramp
cargo run --bin create --release -- --name regf-bydelta-relumin-expramp-p2 regf -w relumin -p expramp-p2
cargo run --bin create --release -- --name regf-bydelta-relumin-expramp-p4 regf -w relumin -p expramp-p4

cargo run --bin create --release -- --name regf-bydelta-softmin-expramp-n4 regf -w softmin -p expramp-n4
cargo run --bin create --release -- --name regf-bydelta-softmin-expramp-n2 regf -w softmin -p expramp-n2
cargo run --bin create --release -- --name regf-bydelta-softmin-linearramp regf -w softmin -p linearramp
cargo run --bin create --release -- --name regf-bydelta-softmin-expramp-p2 regf -w softmin -p expramp-p2
cargo run --bin create --release -- --name regf-bydelta-softmin-expramp-p4 regf -w softmin -p expramp-p4

cargo run --bin create --release -- --name regf-bydelta-squaremin-expramp-n4 regf -w squaremin -p expramp-n4
cargo run --bin create --release -- --name regf-bydelta-squaremin-expramp-n2 regf -w squaremin -p expramp-n2
cargo run --bin create --release -- --name regf-bydelta-squaremin-linearramp regf -w squaremin -p linearramp
cargo run --bin create --release -- --name regf-bydelta-squaremin-expramp-p2 regf -w squaremin -p expramp-p2
cargo run --bin create --release -- --name regf-bydelta-squaremin-expramp-p4 regf -w squaremin -p expramp-p4

cargo run --bin create --release -- --name regf-bydelta-argmin-expramp-n4 regf -w argmin -p expramp-n4
cargo run --bin create --release -- --name regf-bydelta-argmin-expramp-n2 regf -w argmin -p expramp-n2
cargo run --bin create --release -- --name regf-bydelta-argmin-linearramp regf -w argmin -p linearramp
cargo run --bin create --release -- --name regf-bydelta-argmin-expramp-p2 regf -w argmin -p expramp-p2
cargo run --bin create --release -- --name regf-bydelta-argmin-expramp-p4 regf -w argmin -p expramp-p4

cargo run --bin create --release -- --name regf-bydelta-avgmin-expramp-n4 regf -w avgmin -p expramp-n4
cargo run --bin create --release -- --name regf-bydelta-avgmin-expramp-n2 regf -w avgmin -p expramp-n2
cargo run --bin create --release -- --name regf-bydelta-avgmin-linearramp regf -w avgmin -p linearramp
cargo run --bin create --release -- --name regf-bydelta-avgmin-expramp-p2 regf -w avgmin -p expramp-p2
cargo run --bin create --release -- --name regf-bydelta-avgmin-expramp-p4 regf -w avgmin -p expramp-p4


cargo run --bin perf --release -- --name regf-bydelta-lnmin-expramp-n4
cargo run --bin perf --release -- --name regf-bydelta-lnmin-expramp-n2
cargo run --bin perf --release -- --name regf-bydelta-lnmin-linearramp
cargo run --bin perf --release -- --name regf-bydelta-lnmin-expramp-p2
cargo run --bin perf --release -- --name regf-bydelta-lnmin-expramp-p4

cargo run --bin perf --release -- --name regf-bydelta-linearmin-expramp-n4
cargo run --bin perf --release -- --name regf-bydelta-linearmin-expramp-n2
cargo run --bin perf --release -- --name regf-bydelta-linearmin-linearramp
cargo run --bin perf --release -- --name regf-bydelta-linearmin-expramp-p2
cargo run --bin perf --release -- --name regf-bydelta-linearmin-expramp-p4

cargo run --bin perf --release -- --name regf-bydelta-elumin-expramp-n4
cargo run --bin perf --release -- --name regf-bydelta-elumin-expramp-n2
cargo run --bin perf --release -- --name regf-bydelta-elumin-linearramp
cargo run --bin perf --release -- --name regf-bydelta-elumin-expramp-p2
cargo run --bin perf --release -- --name regf-bydelta-elumin-expramp-p4

cargo run --bin perf --release -- --name regf-bydelta-relumin-expramp-n4
cargo run --bin perf --release -- --name regf-bydelta-relumin-expramp-n2
cargo run --bin perf --release -- --name regf-bydelta-relumin-linearramp
cargo run --bin perf --release -- --name regf-bydelta-relumin-expramp-p2
cargo run --bin perf --release -- --name regf-bydelta-relumin-expramp-p4

cargo run --bin perf --release -- --name regf-bydelta-softmin-expramp-n4
cargo run --bin perf --release -- --name regf-bydelta-softmin-expramp-n2
cargo run --bin perf --release -- --name regf-bydelta-softmin-linearramp
cargo run --bin perf --release -- --name regf-bydelta-softmin-expramp-p2
cargo run --bin perf --release -- --name regf-bydelta-softmin-expramp-p4

cargo run --bin perf --release -- --name regf-bydelta-squaremin-expramp-n4
cargo run --bin perf --release -- --name regf-bydelta-squaremin-expramp-n2
cargo run --bin perf --release -- --name regf-bydelta-squaremin-linearramp
cargo run --bin perf --release -- --name regf-bydelta-squaremin-expramp-p2
cargo run --bin perf --release -- --name regf-bydelta-squaremin-expramp-p4

cargo run --bin perf --release -- --name regf-bydelta-argmin-expramp-n4
cargo run --bin perf --release -- --name regf-bydelta-argmin-expramp-n2
cargo run --bin perf --release -- --name regf-bydelta-argmin-linearramp
cargo run --bin perf --release -- --name regf-bydelta-argmin-expramp-p2
cargo run --bin perf --release -- --name regf-bydelta-argmin-expramp-p4

cargo run --bin perf --release -- --name regf-bydelta-avgmin-expramp-n4
cargo run --bin perf --release -- --name regf-bydelta-avgmin-expramp-n2
cargo run --bin perf --release -- --name regf-bydelta-avgmin-linearramp
cargo run --bin perf --release -- --name regf-bydelta-avgmin-expramp-p2
cargo run --bin perf --release -- --name regf-bydelta-avgmin-expramp-p4
